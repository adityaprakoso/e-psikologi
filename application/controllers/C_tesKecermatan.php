<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_tesKecermatan extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->model('m_paket_soal');
		$this->load->model('m_pertanyaan');
		$this->load->model('m_bank_soal');
		$this->load->model('m_pengaturan');
		$this->load->library('form_validation');
		
    }

	public function index()
	{
		$this->load->view('tmp_user/header');
		$this->load->view('index');
		$this->load->view('tmp_user/footer');
	}
	
	public function getDataPertanyaan()
	{
		$paket_soal = $this->input->post('paket_soal');
		$kolom_pertanyaan = $this->input->post('kolom_pertanyaan');
		$data['data_pertanyaan'] = $this->m_pertanyaan->get_data_pertanyaan('id_paket_soal', $paket_soal, $kolom_pertanyaan)->result_array();
		// $data['data_pertanyaan'] = $this->m_pertanyaan->get_data_pertanyaan('id_paket_soal', $paket_soal)->row_array($kolom_pertanyaan);
		$data['jumlah_pertanyaan'] = $this->m_pertanyaan->get_jumlah_pertanyaan('id_paket_soal', $paket_soal);
		echo json_encode($data);
	}

	public function getDataSoal()
	{
		$id_pertanyaan = $this->input->post('id_pertanyaan');
		$nomor_soal = $this->input->post('nomor_soal');
		$data['data_soal'] = $this->m_bank_soal->get_data_soal('id_pertanyaan', $id_pertanyaan, $nomor_soal)->result_array();
		// $data['data_soal'] = $this->m_bank_soal->get_data_soal('id_pertanyaan', $id_pertanyaan)->row_array($nomor_soal);
		$data['jumlah_soal'] = $this->m_bank_soal->get_jumlah_soal('id_pertanyaan', $id_pertanyaan);
		echo json_encode($data);
	}

	public function teskecermatan()
	{
		$this->load->view('tmp_user/header');
		$this->load->view('jenistes/v_teskecermatan');
		$this->load->view('tmp_user/footer');
	}
	public function soal($paket_soal, $id_pertanyaan=null, $kolom_pertanyaan=null, $nilai=0)
	{
		$dataHeader['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$data['paket_soal'] = $paket_soal;
		$data['data_pertanyaan'] = $this->m_pertanyaan->get_where_by('id_paket_soal', $paket_soal)->row_array();
		
		if($id_pertanyaan!=null) {
			$id_pertanyaan = $id_pertanyaan;
			$data['kolom_pertanyaan'] = $kolom_pertanyaan;
			$data['data_pertanyaan'] = $this->m_pertanyaan->get_where_by('id_pertanyaan', $id_pertanyaan)->row_array();
			$data['nilai'] = $nilai;
		}
		else $id_pertanyaan = $data['data_pertanyaan']['id_pertanyaan'];

		$data['data_soal'] = $this->m_bank_soal->get_data_soal('id_pertanyaan', $id_pertanyaan)->row_array();
		$this->load->view('tmp_user/header', $dataHeader);
		$this->load->view('jenistes/v_soalkecermatan', $data);
		$flag['flag'] = 'tes_kecermatan';
		$this->load->view('tmp_user/footer', $flag);
	}
}