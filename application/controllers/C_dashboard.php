<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_dashboard extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->model('m_paket_soal');
		$this->load->model('m_pertanyaan');
		$this->load->model('m_bank_soal');
		$this->load->model('m_pengaturan');
		$this->load->model('m_kepribadian');
		$this->load->model('m_nilai');
		$this->load->library('form_validation');
		
    }

	public function index()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$this->load->view('tmp_user/header',$data);
		$this->load->view('index' ,$data);
		$this->load->view('tmp_user/footer');
	}
	
	public function ujian()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$this->load->view('tmp_user/header' ,$data);
		$this->load->view('v_ujian',$data);
		$this->load->view('tmp_user/footer');
	}

	public function jenistes()
	{

		$kode_ujian = $this->input->post('kode_ujian');

		$data['data_kelas'] = $this->m_pengaturan->get_kelas_by_code($kode_ujian);

		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		if(!empty($data['data_kelas'])){
			$this->session->set_userdata('id_kelas', $data['data_kelas']->id_kelas);
			$this->load->view('tmp_user/header' ,$data);
			$this->load->view('v_jenistes', $data);
			$this->load->view('tmp_user/footer');
		}
		else{
			$this->ujian();
		}
	}

	public function teskecerdasan($kode_ujian)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$data['kode_ujian'] = $kode_ujian;
		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_teskecerdasan', $data);
		$this->load->view('tmp_user/footer');
	}

	public function teskecermatan($paket_soal)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		//get waktu
		$data['waktu'] = $this->m_paket_soal->get_waktu('waktu_tes_kecermatan', $paket_soal)->row();
		$data['waktu'] = $data['waktu']->waktu * 60;
		
		$pertanyaan = $this->m_pertanyaan->get_where_by('id_paket_soal', $paket_soal)->result_array();
		
		//Get all (60) soal
		for ($i=0; $i < count($pertanyaan); $i++) { 

			$soal = $this->m_bank_soal->get_where('id_pertanyaan', $pertanyaan[$i]['id_pertanyaan'])->result_array();
			// $waktu = $pertanyaan[$i]['waktu'];
			$isi_kolom= $pertanyaan[$i]['kolom_a'].",".
			$pertanyaan[$i]['kolom_b'].",".
			$pertanyaan[$i]['kolom_c'].",".
			$pertanyaan[$i]['kolom_d'].",".
			$pertanyaan[$i]['kolom_e'];
			
			for ($j=0; $j < count($soal); $j++) { 

				$isi_soal= $soal[$j]['data_1'].",".
				$soal[$j]['data_2'].",".
				$soal[$j]['data_3'].",".
				$soal[$j]['data_4'];

				$kolom[$i+1][$j+1] = 
				[
					"id_pertanyaan" => $pertanyaan[$i]['id_pertanyaan'],
					"id_soal" => $soal[$j]['id_bank_soal'],
					"isi_kolom" => $isi_kolom,
					"soal" => $isi_soal,
					"jawaban" => NULL,
				];

			}
		}

		$data['data_kolom'] = [
			// 'user' => $data['user'],
			'waktu' => $data['waktu'],
			'data' => $kolom
		];

		$data['data_kolom'] = json_encode($data['data_kolom']);

		// var_dump($data['data_kolom']);
		// die();

		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_teskecermatan');
		$this->load->view('tmp_user/footer');
	}

	public function hasilTes($nilai)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$data['nilai'] = $nilai;
		$id_login = $this->session->userdata('id_login');
		$id_kelas = $this->session->userdata('id_kelas');

		//proses insert data nilai
		$data_nilai = array(
			'id_login'  	=> $id_login,
			'id_kelas'  	=> $id_kelas,
			'total'  		=> $nilai
		);
		$prosesInsertNilai = $this->m_nilai->insert_nilai($data_nilai);

		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_hasilteskecermatan', $data);
		$this->load->view('tmp_user/footer');
	}

	public function prosesHasilTesKecermatan()
	{
		$_POST = json_decode($this->input->raw_input_stream, true);
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$id_user = $data['user']['id_login'];
		$id_kelas = $this->session->userdata('id_kelas');
		$data_jawaban = $_POST;
		$poin = 0;
		$jumlah_benar = 0;
		$jumlah_salah = 0;
		// var_dump($data_jawaban);die;
		//Get all (60) soal
		for ($i=0; $i < count($data_jawaban); $i++) { 

			for ($j=1; $j <= count($data_jawaban[$i]); $j++) { 
				
				$id_pertanyaan = $data_jawaban[$i][$j]['id_pertanyaan'];
				$id_soal = $data_jawaban[$i][$j]['id_soal'];
				$jawaban_user = $data_jawaban[$i][$j]['jawaban'];
				
				$data_soal = $this->m_bank_soal->get_where('id_bank_soal', $id_soal)->row_array();
				$jawaban_soal = $data_soal['jawab_benar'];
				// var_dump($data_soal);die;

				if($jawaban_user == $jawaban_soal){
					// $poin += $data_soal['bobot'];
					$jumlah_benar++;
				} else $jumlah_salah++;

			}

		}
		// var_dump($tes_jawaban);
		// die;

		$poin = $jumlah_benar - $jumlah_salah;
		if($poin <0) $poin = 0;

		//proses insert data nilai
		$data_nilai = array(
			'id_login'  	=> $id_user,
			'id_kelas'  	=> $id_kelas,
			'jenis_tes'  	=> "kecermatan",
			'jumlah_benar'  	=> $jumlah_benar,
			'jumlah_salah'  	=> $jumlah_salah,
			'total'  		=> $poin
		);
		// var_dump($data_nilai);die;

		$prosesInsertNilai = $this->m_nilai->insert_nilai($data_nilai);
		if($prosesInsertNilai) $data = ['status' => true,'id_nilai' => $prosesInsertNilai, 'mesage' => 'Nilai berhasil disimpan.'];
		else $data = ['status' => false, 'mesage' => 'Nilai gagal disimpan.'];

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function prosesHasilTesKepribadian()
	{
		$_POST = json_decode($this->input->raw_input_stream, true);
		$id_user = $_POST['id_user'];
		$id_kelas = $this->session->userdata('id_kelas');
		$data_pertanyaan = $_POST['pertanyaan'];
		$poin = 0;
		$jumlah_benar = 0;
		$jumlah_salah = 0;

		foreach ($data_pertanyaan as $value) {
			$data_soal =  $this->m_kepribadian->get_where('id_kepribadian', $value['id_pertanyaan'])->row_array();

			if($value['jawaban_dipilih'] == $data_soal['jawab']){
				$poin += $data_soal['bobot'];
				$jumlah_benar++;
			} else $jumlah_salah++;
		}

		//proses insert data nilai
		$data_nilai = array(
			'id_login'  	=> $id_user,
			'id_kelas'  	=> $id_kelas,
			'jenis_tes'  	=> "kepribadian",
			'jumlah_benar'  	=> $jumlah_benar,
			'jumlah_salah'  	=> $jumlah_salah,
			'total'  		=> $poin
		);

		$prosesInsertNilai = $this->m_nilai->insert_nilai($data_nilai);
		if($prosesInsertNilai) $data = ['status' => true,'id_nilai' => $prosesInsertNilai, 'mesage' => 'Nilai berhasil disimpan.'];
		else $data = ['status' => false, 'mesage' => 'Nilai gagal disimpan.'];

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function hasilTesKecermatan()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();

		$id_nilai = $this->input->get('id_nilai');
		$data['nilai'] = $this->m_nilai->get_where('id_nilai', $id_nilai)->row_array();

		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_hasilteskecermatan', $data);
		$this->load->view('tmp_user/footer');
	}

	public function hasilTesKepribadian()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();

		$id_nilai = $this->input->get('id_nilai');
		$data['nilai'] = $this->m_nilai->get_where('id_nilai', $id_nilai)->row_array();

		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_hasilteskepribadian', $data);
		$this->load->view('tmp_user/footer');
	}

	public function soal($paket_soal=null)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		
		$data['paket_soal'] = $paket_soal;
		$data['data_pertanyaan'] = $this->m_pertanyaan->get_where_by('id_paket_soal', $paket_soal)->row_array();
		$data['data_soal'] = $this->m_bank_soal->get_data_soal('id_pertanyaan', $data['data_pertanyaan']['id_pertanyaan'])->row_array();
		$this->load->view('tmp_user/header' ,$data);
		$this->load->view('jenistes/v_soalkecermatan', $data);
		$flag['flag'] = 'tes_kecermatan';
		$this->load->view('tmp_user/footer', $flag);
	}
}