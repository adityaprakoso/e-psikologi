<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // check_not_login();
		$this->load->model('m_login');
		$this->load->library('form_validation');
		
    }

	public function index()
	{
		$this->load->view('login/v_login');
	}
	
	public function register()
	{
		$this->load->view('login/v_register');
	}

    public function proses_register()
	{
			$username	= $this->input->post('username');
			$password	= $this->input->post('password'); 
			$nama	= $this->input->post('nama'); 
			$tgl_lahir	= $this->input->post('tgl_lahir');
			$jenis_kl	= $this->input->post('jenis_kl');
			$agama	= $this->input->post('agama');
			$level	= $this->input->post('level');

			$data = array(
				'username'  	=> $username,
				'password'  		=> $password,
				'nama'  		=> $nama,
				'tgl_lahir'  		=> $tgl_lahir,
				'jenis_kl'  		=> $jenis_kl,
				'agama'  		=> $agama,
				'level'  		=> $level,
			);
			$this->m_login->add_model_login($data, 'tb_login');
			// $this->session->set_flashdata('pesan','Paket Soal Berhasil Di simpan');
			redirect('register');
	}

    public function proses_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$cekdaftar = $this->m_login->cekdaftar($username);
		if($cekdaftar){
			$ceklogin = $this->m_login->ceklogin($username, $password);
			if($ceklogin){
				foreach ($ceklogin as $row);
				if($row->status == "aktif"){
					$this->session->set_userdata('id_login', $row->id_login);
					$this->session->set_userdata('username', $row->username);
					$this->session->set_userdata('nama', $row->nama);
					$this->session->set_userdata('level', $row->level);
					
					if($this->session->userdata('level')=="admin"){
						// redirect('C_admin');
						echo "<script> window.location ='".base_url('C_auth')."';</script>";
					}else{
						// redirect('C_dashboard');
						echo "<script> window.location ='".base_url('C_dashboard')."';</script>";
					}
				}else{
					echo "<script>alert('Maaf Username anda belum aktif'); </script>";
					// redirect('C_auth');
					echo "<script> window.location ='".base_url('C_auth')."';</script>";
				}
			}else{
				echo "<script>alert('Maaf Username dan Password anda salah'); </script>";
					// redirect('C_auth');
					echo "<script> window.location ='".base_url('C_auth')."';</script>";
			}
		}else{
			echo "<script>alert('Maaf Username anda belum Tedaftar'); </script>";
					// redirect('C_auth');
					echo "<script> window.location ='".base_url('C_auth')."';</script>";
		}
	} 
	// end publik function

	public function logut()
	{
		$this->session->sess_destroy();
		// redirect('c_auth','refresh');
		echo "<script> window.location ='".base_url('C_auth')."';</script>";
	}

}