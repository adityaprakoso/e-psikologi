<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kepribadian extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->model('m_paket_soal');
		$this->load->model('m_pertanyaan');
		$this->load->model('m_bank_soal');
		$this->load->model('m_pengaturan');
		$this->load->model('m_kepribadian');
		$this->load->model('m_nilai');
		$this->load->library('form_validation');
		
    }

	public function index()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$this->load->view('tmp_user/header',$data);
		$this->load->view('index' ,$data);
		$this->load->view('tmp_user/footer');
	}

	public function instruksiKepribadian($kode_ujian)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		$data['kode_ujian'] = $kode_ujian;
		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_instruksiKepribadian', $data);
		$this->load->view('tmp_user/footer');
	}

	public function teskepribadian($paket_soal)
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();
		
		// $data['data_soal'] = $this->db->query("SELECT * FROM tb_kepribadian tr, tb_paket_soal tp WHERE tr.id_paket_soal= tp.id_paket_soal limit 5")->result();
		
		$data['paket_soal'] = $paket_soal;
		$data_pertanyaan = $this->m_kepribadian->get_where('id_paket_soal', $paket_soal)->result();
		$data['waktu'] = $this->m_paket_soal->get_waktu('waktu_tes_kepribadian', $paket_soal)->row();
		$data['waktu'] = $data['waktu']->waktu * 60;
		
		for ($i=0; $i < count($data_pertanyaan); $i++) { 
			$data['data_pertanyaan'][$i]['id_soal'] = $data_pertanyaan[$i]->id_kepribadian;
			$data['data_pertanyaan'][$i]['nomor'] = $i+1;
			$data['data_pertanyaan'][$i]['soal'] = $data_pertanyaan[$i]->soal;
			$data['data_pertanyaan'][$i]['bobot'] = $data_pertanyaan[$i]->bobot;
			// $data['data_pertanyaan'][$i]['jawabanBenar'] = $data_pertanyaan[$i]->jawab;
			$data['data_pertanyaan'][$i]['jawabanDipilih'] = '-';
			$data['data_pertanyaan'][$i]['gambar'] = explode(',', $data_pertanyaan[$i]->gambar);
			if($data['data_pertanyaan'][$i]['gambar'] == [""]) $data['data_pertanyaan'][$i]['gambar'] = [];
			$data['data_pertanyaan'][$i]['opsi'] = [];
			array_push($data['data_pertanyaan'][$i]['opsi'], 
			$data_pertanyaan[$i]->op_a,
			$data_pertanyaan[$i]->op_b,
			$data_pertanyaan[$i]->op_c,
			$data_pertanyaan[$i]->op_d,
			$data_pertanyaan[$i]->op_e);
		}
		// $data['gambar'] = explode(',', $data['data_pertanyaan'][0]->gambar);
		$data['data_pertanyaan'] = json_encode($data['data_pertanyaan']);
		// var_dump($data);
		// die();
		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_teskepribadian',$data);
		$this->load->view('tmp_user/footer');
	}

	public function prosesHasilTesKepribadian()
	{
		$_POST = json_decode($this->input->raw_input_stream, true);
		$id_user = $_POST['id_user'];
		$id_kelas = $this->session->userdata('id_kelas');
		$data_pertanyaan = $_POST['pertanyaan'];
		$poin = 0;
		$jumlah_benar = 0;
		$jumlah_salah = 0;

		foreach ($data_pertanyaan as $value) {
			$data_soal =  $this->m_kepribadian->get_where('id_kepribadian', $value['id_pertanyaan'])->row_array();

			if($value['jawaban_dipilih'] == $data_soal['jawab']){
				$poin += $data_soal['bobot'];
				$jumlah_benar++;
			} else $jumlah_salah++;
		}

		//proses insert data nilai
		$data_nilai = array(
			'id_login'  	=> $id_user,
			'id_kelas'  	=> $id_kelas,
			'jenis_tes'  	=> "kepribadian",
			'jumlah_benar'  	=> $jumlah_benar,
			'jumlah_salah'  	=> $jumlah_salah,
			'total'  		=> $poin
		);

		$prosesInsertNilai = $this->m_nilai->insert_nilai($data_nilai);
		if($prosesInsertNilai) $data = ['status' => true,'id_nilai' => $prosesInsertNilai, 'mesage' => 'Nilai berhasil disimpan.'];
		else $data = ['status' => false, 'mesage' => 'Nilai gagal disimpan.'];

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function hasilTesKepribadian()
	{
		$data['user'] = $this->db->get_where('tb_login',['username' => $this->session->userdata('username','nama','tgl_lahir')])->row_array();

		$id_nilai = $this->input->get('id_nilai');
		$data['nilai'] = $this->m_nilai->get_where('id_nilai', $id_nilai)->row_array();

		$this->load->view('tmp_user/header',$data);
		$this->load->view('jenistes/v_hasilteskepribadian', $data);
		$this->load->view('tmp_user/footer');
	}
}