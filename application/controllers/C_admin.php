<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        // check_not_login();
		$this->load->model('m_paket_soal');
		$this->load->model('m_pertanyaan');
		$this->load->model('m_bank_soal');
		$this->load->model('m_kepribadian');
		$this->load->model('m_pengaturan');
		$this->load->library('form_validation');
		
    }

	public function index()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Dashbord');
		$this->load->view('template-admin/footer');
	}

	// untuk paket Soal

	public function paketsoal()
	{
		$data['get_paket_soal'] = $this->m_paket_soal->get_paket_soal()->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Paketsoal' ,$data);
		$this->load->view('template-admin/footer');
	}

	public function add_paket_soal()
	{
			$nama_tes	= $this->input->post('nama_tes');
			$jenis_tes	= $this->input->post('jenis_tes');
			$data = array(
				'nama_tes' => $nama_tes,
				'jenis_tes'  => $jenis_tes,
			);
			$this->m_paket_soal->add_model_paket_soal($data, 'tb_paket_soal');
			$this->session->set_flashdata('pesan','Paket Soal Berhasil Di simpan');
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
			echo "<script> window.location ='".base_url('C_admin/Paketsoal')."';</script>";
	}

	public function edit_paket_soal($id)
	{
		$where = array('id_paket_soal' => $id);
		$data['get_id'] = $this->m_paket_soal->get_id($where,'tb_paket_soal')->result();
		
		$this->load->view('template-admin/header');
		$this->load->view('Admin/edit_paket_soal', $data);
		$this->load->view('template-admin/footer');
	}
	
	public function prose_edit_paket_soal()
	{
			$id_paket_soal	= $this->input->post('id_paket_soal');
			$nama_tes	= $this->input->post('nama_tes');
			$jenis_tes	= $this->input->post('jenis_tes');

			$data = array(
				'nama_tes' => $nama_tes,
				'jenis_tes'  => $jenis_tes,
			);
			$where = array(
				'id_paket_soal' => $id_paket_soal
			);
	
			$this->m_paket_soal->update_data('tb_paket_soal', $data, $where);
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
			echo "<script> window.location ='".base_url('C_admin/Paketsoal')."';</script>";
	}

	// function hapus_paket_soal($id)
    // {
    //     $where = array('id_paket_soal' => $id);
    //     $this->m_paket_soal->hapus_id_paket_soal($where);
	// 	redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
	// }

	public function hapus_paket_soal($id_paket_soal){
		$this->m_paket_soal->hapus_id_paket_soal($id_paket_soal);
		// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
		echo "<script> window.location ='".base_url('C_admin/Paketsoal')."';</script>";
	}
	// end proses paket Soals


	public function tampil_pertanyaan($id)
	{
		$data['get_id'] = $this->m_pertanyaan->get_id()->result();
		
		// $data['paket_soal'] = $paket_soal;
		$data['data_pertanyaan'] = $this->m_pertanyaan->get_where($id)->result();
		// untuk tampil

		// untuk ambil id
		$where = array('id_paket_soal' => $id);
		$data['get_id'] = $this->m_paket_soal->get_id($where,'tb_paket_soal')->result();

		// $data['data_pertanyaan'] = $this->db->query("SELECT * FROM tb_pertanyaan, tb_paket_soal,
		// where tb_pertanyaan.id_paket_soal = tb_paket_soal.id_paket_soal AND tb_pertanyaan.id_paket_soal='$id'")->result();

		$this->load->view('template-admin/header');
		$this->load->view('Admin/pertanyaan/v_pertanyaan', $data);
		$this->load->view('template-admin/footer');
	}

	public function edit_pertanyaan($id)
	{
		$where = array('id_pertanyaan' => $id);
		$data['edit_pertanyaan'] = $this->m_pertanyaan->edit_pertanyaan($where,'tb_pertanyaan')->result();

		$this->load->view('template-admin/header');
		$this->load->view('Admin/pertanyaan/edit_pertanyaan',$data);
		$this->load->view('template-admin/footer');
	}
	public function edit_proses_pertanyaan ()
	{
			$id_pertanyaan	= $this->input->post('id_pertanyaan');
			$deskripsi_pertanyaan	= $this->input->post('deskripsi_pertanyaan');
			$kolom_a	= $this->input->post('kolom_a');
			$kolom_b	= $this->input->post('kolom_b');
			$kolom_c	= $this->input->post('kolom_c');
			$kolom_d	= $this->input->post('kolom_d');
			$kolom_e	= $this->input->post('kolom_e');
			$waktu	= $this->input->post('waktu');

			$data = array(
				'deskripsi_pertanyaan'  	=> $deskripsi_pertanyaan,
				'kolom_a'  		=> $kolom_a,
				'kolom_b'  		=> $kolom_b,
				'kolom_c'  		=> $kolom_c,
				'kolom_d'  		=> $kolom_d,
				'kolom_e'  		=> $kolom_e,
				'waktu'  		=> $waktu,
			);
			$where = array(
				'id_pertanyaan' => $id_pertanyaan
			);
	
			$this->m_pertanyaan->update_data('tb_pertanyaan', $data, $where);
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
			echo "<script> window.location ='".base_url('C_admin/tampil_pertanyaan/'.$id_paket_soal)."';</script>";
	}

	public function hapus_pertanyaan($id){
		$this->m_pertanyaan->hapus_pertanyaan($id);
		// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
		echo "<script> window.location ='".base_url('C_admin/tampil_pertanyaan/'.$id_paket_soal)."';</script>";
	}

	public function add_proses_pertanyaan()
	{
			$id_paket_soal	= $this->input->post('id_paket_soal');
			$deskripsi_pertanyaan	= $this->input->post('deskripsi_pertanyaan');
			$kolom_a	= $this->input->post('kolom_a');
			$kolom_b	= $this->input->post('kolom_b');
			$kolom_c	= $this->input->post('kolom_c');
			$kolom_d	= $this->input->post('kolom_d');
			$kolom_e	= $this->input->post('kolom_e');
			$waktu	= $this->input->post('waktu');

			$data = array(
				'id_paket_soal' => $id_paket_soal,
				'deskripsi_pertanyaan'  	=> $deskripsi_pertanyaan,
				'kolom_a'  		=> $kolom_a,
				'kolom_b'  		=> $kolom_b,
				'kolom_c'  		=> $kolom_c,
				'kolom_d'  		=> $kolom_d,
				'kolom_e'  		=> $kolom_e,
				'waktu'  		=> $waktu,
			);
			$this->m_pertanyaan->add_model_pertanyaan($data, 'tb_pertanyaan');
			// echo "<script> alert('Berhasil Disimpan');</script>";
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal/');
			echo "<script> window.location ='".base_url('C_admin/tampil_pertanyaan/'.$id_paket_soal)."';</script>";
			
	}

	// proses Bank Soal
	public function tampil_bank_soal($id)
	{
		$data['get_bank_soal'] = $this->m_bank_soal->get_bank_soal($id)->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/bank_soal/v_bank_soal', $data);
		$this->load->view('template-admin/footer');
	}

	public function add_proses_bank_soal()
	{
			$id_pertanyaan	= $this->input->post('id_pertanyaan');
			$data_1	= $this->input->post('data_1');
			$data_2	= $this->input->post('data_2');
			$data_3	= $this->input->post('data_3');
			$data_4	= $this->input->post('data_4');
			$jawab_benar	= $this->input->post('jawab_benar');

			$data = array(
				'id_pertanyaan' => $id_pertanyaan,
				'data_1'  	=> $data_1,
				'data_2'  		=> $data_2,
				'data_3'  		=> $data_3,
				'data_4'  		=> $data_4,
				'jawab_benar'  		=> $jawab_benar,
			);
			$this->m_bank_soal->add_model_bank_soal($data, 'tb_bank_soal');
			// $this->session->set_flashdata('pesan','Paket Soal Berhasil Di simpan');
			echo "<script> window.location ='".base_url('C_admin/tampil_pertanyaan/'.$id_paket_soal)."';</script>";
	}

	public function edit_bank_soal($id)
	{
		$where = array('id_bank_soal' => $id);
		$data['edit_bank_soal'] = $this->m_bank_soal->edit_bank_soal($where,'tb_bank_soal')->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/bank_soal/edit_bank_soal',$data);
		$this->load->view('template-admin/footer');
	}

	public function edit_proses_bank_soal()
	{
			$id_bank_soal	= $this->input->post('id_bank_soal');
			$data_1	= $this->input->post('data_1');
			$data_2	= $this->input->post('data_2');
			$data_3	= $this->input->post('data_3');
			$data_4	= $this->input->post('data_4');
			$jawab_benar	= $this->input->post('jawab_benar');

			$data = array(
				'data_1'  	=> $data_1,
				'data_2'  		=> $data_2,
				'data_3'  		=> $data_3,
				'data_4'  		=> $data_4,
				'jawab_benar'  		=> $jawab_benar,
			);
			$where = array(
				'id_bank_soal' => $id_bank_soal
			);
	
			$this->m_bank_soal->update_data('tb_bank_soal', $data, $where);
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
			echo "<script> window.location ='".base_url('C_admin/Paketsoal')."';</script>";
	}

	public function hapus_bank_soal ($id)
	{
		$this->m_bank_soal->hapus_bank_soal($id);
		// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal');
		echo "<script> window.location ='".base_url('C_admin/Paketsoal')."';</script>";
	}
	// end Proses bank Soal


	// Pengaturan atau jadwal soal 
	public function pengaturansoal()
	{
		$data['get_paket_soal'] = $this->m_paket_soal->get_paket_soal()->result();
		$data['get_pengaturan'] = $this->m_pengaturan->get_pengaturan()->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Pengaturansoal' ,$data);
		$this->load->view('template-admin/footer');
	}

	public function add_proses_pengaturan()
	{
			$id_paket_soal	= $this->input->post('id_paket_soal');
			$kelas			= $this->input->post('kelas');
			$deskripsi		= $this->input->post('deskripsi');
			$code	= $this->input->post('code');
			$tgl_mulai	= $this->input->post('tgl_mulai');
			$tgl_selesai	= $this->input->post('tgl_selesai');

			$data = array(
				'id_paket_soal' => $id_paket_soal,
				'kelas' 		=> $kelas,
				'deskripsi'  	=> $deskripsi,
				'code'  		=> $code,
				'tgl_mulai'  		=> $tgl_mulai,
				'tgl_selesai'  		=> $tgl_selesai,
			);
			$this->m_pengaturan->add_model_pengaturan($data, 'tb_kelas');
			// $this->session->set_flashdata('pesan','Paket Soal Berhasil Di simpan');
			// redirect('C_admin/pengaturansoal');
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/pengaturansoal','refresh');
			echo "<script> window.location ='".base_url('C_admin/pengaturansoal')."';</script>";
	}
	public function edit_kelas($id)
	{
		$where = array('id_kelas' => $id);
		$data['edit_kelas'] = $this->m_pengaturan->edit_kelas($where,'tb_kelas')->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/edit_kelas',$data);
		$this->load->view('template-admin/footer');
	}
	public function prose_edit_kelas()
	{
		$id_kelas	= $this->input->post('id_kelas');
		$kelas			= $this->input->post('kelas');
		$deskripsi		= $this->input->post('deskripsi');
		$code	= $this->input->post('code');
		$tgl_mulai	= $this->input->post('tgl_mulai');
		$tgl_selesai	= $this->input->post('tgl_selesai');
		
		$data = array(
			'kelas' 		=> $kelas,
			'deskripsi'  	=> $deskripsi,
			'code'  		=> $code,
			'tgl_mulai'  		=> $tgl_mulai,
			'tgl_selesai'  		=> $tgl_selesai,
		);

		$where = array(
			'id_kelas' => $id_kelas
		);

		$this->m_pengaturan->update_data('tb_kelas', $data, $where);
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/pengaturansoal');
			echo "<script> window.location ='".base_url('C_admin/pengaturansoal')."';</script>";
	}

	public function hapus_kelas($id)
	{
		$this->m_pengaturan->hapus_kelas($id);
		// redirect('http://10.10.10.5/e-psikologi_project/C_admin/pengaturansoal');
		echo "<script> window.location ='".base_url('C_admin/pengaturansoal')."';</script>";
	}
	// end proses pengaturan atau jadwal soal

	public function tambahpaketsoal()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/tambahpaketsoal');
		$this->load->view('template-admin/footer');
	}
	public function tambahpengaturansoal()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/tambahpengaturansoal');
		$this->load->view('template-admin/footer');
	}
	public function ujian()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/form');
		$this->load->view('template-admin/footer');
	}

	public function tes_kepribadian ()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/tes_kepribadian/v_tampil_kepribadian');
		$this->load->view('template-admin/footer');
	}

	public function proses_add_kepribadian ()
	{
			$id_paket_soal	= $this->input->post('id_paket_soal');
			$bobot			= $this->input->post('bobot');
			$soal		= $this->input->post('soal');
			$op_a	= $this->input->post('op_a');
			$op_b	= $this->input->post('op_b');
			$op_c	= $this->input->post('op_c');
			$op_d	= $this->input->post('op_d');
			$op_e	= $this->input->post('op_e');
			$jawab	= $this->input->post('jawab');

			$data = array(
				'id_paket_soal' => $id_paket_soal,
				'bobot' 		=> $bobot,
				'soal'  		=> $soal,
				'op_a'  		=> $op_a,
				'op_b'  		=> $op_b,
				'op_c'  		=> $op_c,
				'op_d'  		=> $op_d,
				'op_e'  		=> $op_e,
				'jawab'  		=> $jawab,
			);
			$this->m_kepribadian->add_data($data, 'tb_kepribadian');
			// $this->session->set_flashdata('pesan','Paket Soal Berhasil Di simpan');
			// redirect('C_admin/Paketsoal');
			// redirect('http://10.10.10.5/e-psikologi_project/C_admin/Paketsoal','refresh');
			echo "<script> window.location ='".base_url('C_admin/pengaturansoal')."';</script>";
	}
	
}