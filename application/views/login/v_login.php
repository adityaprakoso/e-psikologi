<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>E-Psikologi Login</title>
    <meta name="description" content="" />

    <!-- view port to detect device width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- boostrap framework main css file -->
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/helperr.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/styless.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/responsive.css" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />

    <script src="<?=base_url()?>assets-user/js/modernizr-2.8.3.min.js"></script>
</head>

<body class="main-body">
    <main class="flex-container">
        <!-- signin form start here -->
        <section class="signin">
            <form id="signInForm" method="post" action="<?=base_url('C_auth/proses_login')?>">
                <text class="title">Silahkan login</text>
                <text class="sub-title">selamat datang di aplikasi E-Psikologi</text>

                <div class="form-group inputs">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" />
                </div>

                <div class="form-group inputs">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                </div>

                <div class="form-group">
                    <button type="submit">masuk</button>
                </div>
                <div class="form-group"> <small class="font-weight-bold">Belum punya Akun? <a class="text-danger"
                            href="<?=base_url('c_auth/register')?>">Daftar</a></small> </div>
            </form>
        </section>
    </main>

    <!-- javascript part start here -->
    <script src="<?=base_url()?>assets-user/js/jquery-1.12.0.min.js"></script>
    <script src="<?=base_url()?>assets-user/js/jquery.validate.min.js"></script>
    <!-- <script type="text/javascript">
    $("#signInForm").validate({
        rules: {
            username: "required",
            password: "required",
        },
        messages: {
            username: "Required Field",
            password: "Required Field",
        },

        highlight: function(element) {
            //add error class
            $(element).addClass("inputError");
        },
        unhighlight: function(element) {
            //remove error class
            $(element).removeClass("inputError");
        },
        submitHandler: function() {
            //get the values of sign in form
            var dataString = $("#signInForm").serialize();

            //sample ajax form to send the data
            $.ajax({
                type: "POST",
                url: "#",
                data: dataString,
                success: function(msg) {},
                error: function() {},
            });

            return false;
        },
    });
    </script> -->
</body>

</html>