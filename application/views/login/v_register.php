<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>E-Psikologi Login</title>
    <meta name="description" content="" />

    <!-- view port to detect device width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- boostrap framework main css file -->
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/helperr.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/styless.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets-user/css/responsive.css" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />

    <script src="<?=base_url()?>assets-user/js/modernizr-2.8.3.min.js"></script>
</head>

<br>
<br>
<br>
<body class="main-body">
    <main class="flex-container">
        <!-- signin form start here -->
        <section class="signin">
        <form id="signInForm" method="post" action="<?=base_url('c_auth/proses_register')?>">
                <text class="title">Silahkan login</text>
                <text class="sub-title">selamat datang di aplikasi E-Psikologi</text>

                <div class="form-group inputs">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" />
                </div>

                <div class="form-group inputs">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                </div>

                <div class="form-group inputs">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" />
                </div>

                
                <div class="form-group inputs">
                    <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" />
                </div>

                <div class="form-group inputs">
                    <input type="text" class="form-control" id="jenis_kl" name="jenis_kl" placeholder="Jenis Kelamin" />
                </div>


                <div class="form-group inputs">
                    <input type="text" class="form-control" id="agama" name="agama" placeholder="Agama" />
                </div>


                <div class="form-group inputs">
                    <input type="hidden" class="form-control" id="level" name="level" value='peserta' />
                </div>

                <div class="form-group">
                    <button type="submit">masuk</button>
                </div>
                <div class="form-group"> <small class="font-weight-bold">Sudah punya akun? <a class="text-danger"
                            href="<?=base_url('c_auth')?>">login</a></small> </div>
            </form>
        </section>
    </main>
    <script src="<?=base_url()?>assets-user/js/jquery-1.12.0.min.js"></script>
    <script src="<?=base_url()?>assets-user/js/jquery.validate.min.js"></script>
 
</body>

</html>