   <br><br><br>
   <!-- ======= Footer ======= -->
   <footer class="footer">



       <div class="copyrights">
           <div class="container">
               <p>&copy; E-Psikologi</p>
               <div class="credits">

                   Designed by <a href="https://www.instagram.com/multitekno_/">multitekno</a>
               </div>
           </div>
       </div>

   </footer><!-- End  Footer -->

   <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

   <!-- Vendor JS Files -->
   <script src="<?=base_url()?>assets-user/landingpage/vendor/jquery/jquery.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/jquery.easing/jquery.easing.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/php-email-form/validate.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/modal-video/js/modal-video.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/owl.carousel/owl.carousel.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/superfish/superfish.min.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/hoverIntent/hoverIntent.js"></script>
   <script src="<?=base_url()?>assets-user/landingpage/vendor/aos/aos.js"></script>

   <!-- Template Main JS File -->
   <script src="<?=base_url()?>assets-user/landingpage/js/main.js"></script>
   
    <!-- Javascript sesuai tes -->
   <?php if (isset($flag)) {
    switch ($flag) {
        case "tes_kecermatan":
            $this->load->view('tmp_user/ajax_tesKecermatan');
        break;
    }
}
   ?>

   </body>

   </html>
