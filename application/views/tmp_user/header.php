<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>E-psikologi</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?=base_url()?>assets-user/landingpage/img/favicon.pg" rel="icon">
    <link href="<?=base_url()?>assets-user/landingpage/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?=base_url()?>assets-user/landingpage/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets-user/landingpage/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets-user/landingpage/vendor/modal-video/css/modal-video.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets-user/landingpage/vendor/owl.carousel/assets/owl.carousel.min.css"
        rel="stylesheet">
    <link href="<?=base_url()?>assets-user/landingpage/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?=base_url()?>assets-user/landingpage/css/stylesss.css" rel="stylesheet">

	<script src="<?=base_url()?>assets-user/js/sweetalert2.all.min.js"></script>
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="header">
        <div class="container">

            <div id="logo" class="pull-left">
                <h1><a href="<?=base_url('c_dashboard')?>"><span>E-</span>Psikologi</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" title="" /></a>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="<?=base_url('c_dashboard')?>">Home</a></li>

                    <li><a href="<?=base_url('c_dashboard/ujian')?>">Ujian</a></li>
                    
                    <li>
                        <a data-toggle="dropdown">
                            <?=$user['username']?>
                           <span class=”caret”></span>
                        </a>
                        <ul class=”dropdown-menu”>
                        <li><a href="<?=base_url('c_auth/logut')?>">Profil</a></li>
                            <li><a href="<?=base_url('c_auth/logut')?>">Keluar</a></li>
                        </ul>
                    </li>
                   
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- End Header -->
    <br>
    <br>
