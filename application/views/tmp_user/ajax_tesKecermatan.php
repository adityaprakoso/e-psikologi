<script>

//Set waktu ujian
$(document).ready(function(){

  //menyesuaikan nomor kolom
  let kolom_pertanyaan = $("#kolom_pertanyaan").val();
  $("#kolom_pertanyaan").html('Kolom '+(kolom_pertanyaan+1));

  //function timer awal
  setTimeout(function(){
    delayedQuestion();
  },120000); //1000 = 1 detik

});


//function untuk sleep/delay suatu function
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
  }

//function untuk otomatis ganti kolom pertanyaan ketika waktu habis
  async function delayedQuestion() {

  let paket_soal = $("#id_paket_soal").val();
  let kolom_pertanyaan = $("#kolom_pertanyaan").val();
  let jumlah_pertanyaan;

  // === Get data Pertanyaan untuk waktu ujian dan jumlah pertanyaan ===
  $.ajax({
      type: "post",
      url: "<?php echo base_url('C_tesKecermatan/getDataPertanyaan'); ?>",
      async: false,
      data: {
        paket_soal: paket_soal
      },
      success: function(data) {
        // console.log(data);
        data = $.parseJSON(data);
        data_pertanyaan = data.data_pertanyaan[0];
        waktu_ujian = data_pertanyaan.waktu * 60000;
        jumlah_pertanyaan = data.jumlah_pertanyaan-1;
        // console.log(data.data_pertanyaan[kolom_pertanyaan]);
        // var myVar = setInterval(ganti_soal, waktu_ujian);

    }
    });

    for (i = kolom_pertanyaan; i < jumlah_pertanyaan; i++) { //perulangan sebanyak jumlah pertanyaan

        // setTimeout(function(){
        //   kolom_pertanyaan++;
        //   if(kolom_pertanyaan>jumlah_pertanyaan){
        //     let total_nilai = $("#nilai").val();
        //     window.location.href = "<?php echo base_url('C_dashboard/hasilTes/') ?>"+total_nilai;
        //   }
        //   alert('Pertanyaan selanjutnya!');
        //   nextQuestion(paket_soal, kolom_pertanyaan);
        // },2000);
        
        //proses ganti kolom pertanyaan
        kolom_pertanyaan++;
        nextQuestion(paket_soal, kolom_pertanyaan);
        console.log('Loop:'+i);
        await sleep(120000); //1000 = 1 detik
      }

  // // == Jika waktu habis ==
  let total_nilai = $("#nilai").val();
  alert("Waktu habis!");
  window.location.href = "<?php echo base_url('C_dashboard/hasilTes/') ?>"+total_nilai;

  // setTimeout(function(){
  //   let total_nilai = $("#nilai").val();
  //   alert("Waktu habis!");
  //   window.location.href = "<?php echo base_url('C_dashboard/hasilTes/') ?>"+total_nilai;
  // },120000);

  }


var jumlah_soal = 60;

function prosesNext(paket_soal) {

    $('#button_1').html('Next');
    kolom_pertanyaan = $("#kolom_pertanyaan").val();
    nomor_soal = $("#nomor_soal").val();
    let nilai = $("#nilai").val();

    let jawaban = $("input[name='jawaban']:checked").val();
    console.log(jawaban);

    // === Cek jika jawaban kosong ===
    if(jawaban == '' || jawaban == null){
      alert('harapan pilih jawaban!');
      return;
    }

    // === Get Data Pertanyaan ===
    $.ajax({
      type: "post",
      url: "<?php echo base_url('C_tesKecermatan/getDataPertanyaan'); ?>",
      async: false,
      data: {
        paket_soal: paket_soal,
        kolom_pertanyaan: kolom_pertanyaan
      },
      success: function(data) {
        // console.log(data);
        dataPertanyaan = $.parseJSON(data);
        data_pertanyaan = dataPertanyaan.data_pertanyaan[0];
        // data_pertanyaan = dataPertanyaan.data_pertanyaan[kolom_pertanyaan];
        jumlah_pertanyaan = dataPertanyaan.jumlah_pertanyaan-1;
        // console.log(data.data_pertanyaan[kolom_pertanyaan]);
        if(kolom_pertanyaan>jumlah_pertanyaan){
          alert("Pertanyaan Habis");
          return;
        }

        // === Get Data Soal ===
        $.ajax({
        type: "post",
        url: "<?php echo base_url('C_tesKecermatan/getDataSoal'); ?>",
        data: {
            id_pertanyaan: data_pertanyaan.id_pertanyaan,
            nomor_soal: nomor_soal
        },
        success: function(data) {
            data = $.parseJSON(data);
            console.log(data);
            // data_soal = data.data_soal;
            data_soal = data.data_soal[0];
            // console.log(data_soal);
            jumlah_soal = data.jumlah_soal-1;

            // === Proses cek jawaban apakah benar ===
            console.log(data_soal.jawab_benar);
            jawaban = jawaban.toLowerCase();
            data_soal.jawab_benar = data_soal.jawab_benar.toLowerCase();
            if(jawaban == data_soal.jawab_benar){
              // alert('jawaban benar')
              nilai++;
            } else{
              // alert('jawaban salah')
              nilai--;
              if(nilai<0) nilai = 0;
            }
            console.log('nilai: '+nilai)
            $('#nilai').val(nilai); //total nilai

            //Cek jika tombol bernilai selesai
            if($('#button_1').val()=='selesai'){
              window.location.href = "<?php echo base_url('C_dashboard/hasilTes/') ?>"+nilai;
            }

            $("input[name='jawaban']:checked").prop('checked', false);

              // == Jika soal terakhir ==
              if(nomor_soal == (jumlah_soal)){
              kolom_pertanyaan++;
              
              if(kolom_pertanyaan>jumlah_pertanyaan){
                alert("Pertanyaan Habis");
                return;
              }
              
              nomor_soal = $("#nomor_soal").val();
              id_paket_soal = $("#id_paket_soal").val();
              id_pertanyaan = dataPertanyaan.data_pertanyaan[1].id_pertanyaan;
              nilai = $("#nilai").val();
              //ganti kolom soal saat soal habis
              window.location.href = "<?php echo base_url('C_tesKecermatan/soal/') ?>"+id_paket_soal+"/"+id_pertanyaan+"/"+kolom_pertanyaan+"/"+nilai;
              // delayedQuestion();
              // nextQuestion(paket_soal, kolom_pertanyaan);
              return;
            }
            else if((nomor_soal+1) == (jumlah_soal)){
              if(kolom_pertanyaan==jumlah_pertanyaan){
                $('#button_1').html('Selesai');
                $('#button_1').val('selesai');
              }else{
                $('#button_1').html('Next Question');
              }

            }

            data_soal = data.data_soal[1];
            // === Set kolom pertanyaan ===
            $("#kolom_pertanyaan").html('Kolom '+(kolom_pertanyaan+1));
            $('#kolom_a').html(data_pertanyaan.kolom_a);
            $('#kolom_b').html(data_pertanyaan.kolom_b);
            $('#kolom_c').html(data_pertanyaan.kolom_c);
            $('#kolom_d').html(data_pertanyaan.kolom_d);
            $('#kolom_e').html(data_pertanyaan.kolom_e);

            // === Set Soal ===
            $("#nomor_soal").html('Soal '+(nomor_soal+2));
            $('#data_1').html(data_soal.data_1);
            $('#data_2').html(data_soal.data_2);
            $('#data_3').html(data_soal.data_3);
            $('#data_4').html(data_soal.data_4);

            // === Set Jawaban ===
            $('#jawaban_a').val(data_pertanyaan.kolom_a);
            $('#jawaban_b').val(data_pertanyaan.kolom_b);
            $('#jawaban_c').val(data_pertanyaan.kolom_c);
            $('#jawaban_d').val(data_pertanyaan.kolom_d);
            $('#jawaban_e').val(data_pertanyaan.kolom_e);

            $("#nomor_soal").val(nomor_soal+1);
            
        }
        });

          // console.log("jumlah soal: "+jumlah_soal);
          // console.log("Kolom: "+kolom_pertanyaan);
          // console.log("nomor_soal: "+nomor_soal);

      }
    });

    
}

// === Ketika pergantian kolom pertanyaan
function nextQuestion(paket_soal, kolom_pertanyaan) {
    $("#kolom_pertanyaan").val(kolom_pertanyaan); //set kolom pertanyaan
    $("#nomor_soal").val(0); //reset nomor soal
    nomor_soal = $('#nomor_soal').val();

    // === Get Data Pertanyaan ===
    $.ajax({
      type: "post",
      url: "<?php echo base_url('C_tesKecermatan/getDataPertanyaan'); ?>",
      data: {
        paket_soal: paket_soal
      },
      success: function(data) {
        // console.log(data);
        data = $.parseJSON(data);
        data_pertanyaan = data.data_pertanyaan[0];
        jumlah_pertanyaan = data.jumlah_pertanyaan-1;
        // console.log(data.data_pertanyaan[kolom_pertanyaan]);

        // == Jika kolom pertanyaan habis ==


        // === Get Data Soal ===
        $.ajax({
        type: "post",
        url: "<?php echo base_url('C_tesKecermatan/getDataSoal'); ?>",
        data: {
            id_pertanyaan: data_pertanyaan.id_pertanyaan
        },
        success: function(data) {
            data = $.parseJSON(data);
            data_soal = data.data_soal[0];
            jumlah_soal = data.jumlah_soal-1;
            // console.log(data);
            
            // === Set kolom pertanyaan ===
            $("#kolom_pertanyaan").html('Kolom '+(kolom_pertanyaan+1));
            $('#kolom_a').html(data_pertanyaan.kolom_a);
            $('#kolom_b').html(data_pertanyaan.kolom_b);
            $('#kolom_c').html(data_pertanyaan.kolom_c);
            $('#kolom_d').html(data_pertanyaan.kolom_d);
            $('#kolom_e').html(data_pertanyaan.kolom_e);

            // === Set Soal ===
            $("#nomor_soal").html('Soal '+(nomor_soal+1));
            $('#data_1').html(data_soal.data_1);
            $('#data_2').html(data_soal.data_2);
            $('#data_3').html(data_soal.data_3);
            $('#data_4').html(data_soal.data_4);

            // === Set Jawaban ===
            $('#jawaban_a').val(data_pertanyaan.kolom_a);
            $('#jawaban_b').val(data_pertanyaan.kolom_b);
            $('#jawaban_c').val(data_pertanyaan.kolom_c);
            $('#jawaban_d').val(data_pertanyaan.kolom_d);
            $('#jawaban_e').val(data_pertanyaan.kolom_e);
            
        }
        });


    }
    });

}

</script>
