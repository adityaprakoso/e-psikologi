<section id="hero">
    <br>
    <div class="hero-container" data-aos="fade-in">
        <br>
        <h1>MASUKKAN KODE</h1>
        <h2>Masukkan kode yang telah diberikan untuk mengikuti tes</h2>
        <form action="<?=base_url('C_dashboard/jenistes')?>" method="post">
            <div class="form-outline">
                <input id="kode_ujian" name="kode_ujian" type="search" class="form-control" />
                <label class="form-label" for="form1"></label>
            </div>
            <button type="submit" onclick="clearStorage()" class="btn-get-started scrollto">Mulai Ujian</button>
        </form>
    </div>
</section><!-- End Hero Section -->
<script>
    function clearStorage(){
        console.log('storage clear.');

        //Tes kecermatan
        localStorage.removeItem("LAST_KOLOM")
        localStorage.removeItem("LAST_SOAL")

        //Tes kepribadian & kecerdasan
        localStorage.removeItem("LAST_TIME")
        localStorage.removeItem("LAST_ANSWER")
    }
</script>