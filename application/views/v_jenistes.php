<section id="hero">
    <br>
    <div class="hero-container" data-aos="fade-in">
        <br>
        <h1>DESKRIPSI TES</h1>
        <h2>Tes akan dibagi menjadi 3 tes, silahkan pilih tes yang mudah menurut anda untuk dikerjakan lebih dulu</h2>
        <ul class="list-unstyled carousel-indicators">
            <a type="button" class="btn-get-started scrollto" href="<?= base_url('C_kepribadian/instruksiKepribadian/' . $data_kelas->id_paket_soal) ?>">
                Tes Kepribadian
            </a>
            <ul class="list-unstyled carousel-indicators">
                <a type="button" class="btn-get-started scrollto" href="<?= base_url('C_dashboard/teskecerdasan/' . $data_kelas->id_paket_soal) ?>"  >
                    Tes Kecerdasan
                </a>
                <ul class="list-unstyled carousel-indicators">

                    <a type="button" class="btn-get-started scrollto" href="<?= base_url('C_dashboard/teskecermatan/' . $data_kelas->id_paket_soal) ?>">
                        Tes Kecermatan
                    </a>
                </ul>
            </ul>
        </ul>

    </div>
</section><!-- End Hero Section -->