   <footer class="footer">
     © 2021 E-Psikologi.
   </footer>

   </div>
   <!-- End Right content here -->

   </div>
   <!-- END wrapper -->


   <!-- jQuery  -->
   <script src="<?= base_url() ?>assets-admin/assets/js/jquery.min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/popper.min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/bootstrap.min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/modernizr.min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/detect.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/fastclick.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/jquery.slimscroll.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/jquery.blockUI.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/waves.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/jquery.nicescroll.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/js/jquery.scrollTo.min.js"></script>

   <!--Morris Chart-->
   <script src="<?= base_url() ?>assets-admin/assets/plugins/morris/morris.min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/plugins/raphael/raphael-min.js"></script>
   <script src="<?= base_url() ?>assets-admin/assets/pages/crypto-exchange.init.js"></script>

   <!-- App js -->
   <script src="<?= base_url() ?>assets-admin/assets/js/app.js"></script>
   <script type="text/javascript" charset="utf8"
     src="<?= base_url() ?>assets-admin/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" charset="utf8"
     src="<?= base_url() ?>assets-admin/assets/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>

   <script>
$(document).ready(function() {
  $('#datatable').dataTable();
});
   </script>
   </body>

   </html>