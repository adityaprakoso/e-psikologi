<br><br>
<br><br>
<!-- ======= Hero Section ======= -->
<section id="hero">
    <br>
    <div class="hero-container" data-aos="fade-in">
        <br>
        <h1>Selamat datang di aplikasi E-psikologi</h1>
        <h2>Aplikasi E-Psikologi adalah aplikasi ujian sikologi </h2>
        <img src="<?=base_url()?>assets-user/landingpage/img/hero-img.png" alt="Hero Imgs" data-aos="zoom-out"
            data-aos-delay="100">
        <a href="<?=base_url('c_dashboard/ujian')?>" class="btn-get-started scrollto">Mulai Ujian</a>

    </div>
</section><!-- End Hero Section -->