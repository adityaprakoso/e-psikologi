<br>
<br>

<section id="hero">

  <div class="hero-container" data-aos="fade-in">
    <br>
    <br>
    <h1>Tes Kecermatan</h1>
    <input type='hidden' name='id_paket_soal' id="id_paket_soal" value='<?= $data_pertanyaan['id_paket_soal']; ?>' />
    <div class="col-lg-4">
      <div class="card m-b-30 text-white ">
        <div class="card-body">


          <ul style="list-style-type:none;">

            <li id="kolom_pertanyaan" value="<?php if (isset($kolom_pertanyaan)) echo $kolom_pertanyaan;
															else echo '0'; ?>">Kolom 1</li>
            <table class="table">
              <thead>
                <tr>
                  <th>A</th>
                  <th>B</th>
                  <th>C</th>
                  <th>D</th>
                  <th>E</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td scope="row" id="kolom_a"><?= $data_pertanyaan['kolom_a'] ?></td>
                  <td id="kolom_b"><?= $data_pertanyaan['kolom_b'] ?></td>
                  <td id="kolom_c"><?= $data_pertanyaan['kolom_c'] ?></td>
                  <td id="kolom_d"><?= $data_pertanyaan['kolom_d'] ?></td>
                  <td id="kolom_e"><?= $data_pertanyaan['kolom_e'] ?></td>
                </tr>

              </tbody>
            </table>

          </ul>

        </div>

      </div>
      <br>
      <div class="card m-b-30 text-white ">
        <div class="card-body">


          <ul style="list-style-type:none;">

            <li id="nomor_soal" value="0">soal 1</li>
            <table class="table">
              <thead>
                <tr>
                  <th id="data_1"><?= $data_soal['data_1'] ?></th>
                  <th id="data_2"><?= $data_soal['data_2'] ?></th>
                  <th id="data_3"><?= $data_soal['data_3'] ?></th>
                  <th id="data_4"><?= $data_soal['data_4'] ?></th>

                </tr>
              </thead>

            </table>
            <table class="table">

              <tbody>
                <tr>
                  <td scope="row">A
                    <p><input style="height:18px; width:18px;" type='radio' name='jawaban' id="jawaban_a"
                        value='<?= $data_pertanyaan['kolom_a'] ?>' /></p>
                  </td>
                  <td scope="row">B
                    <p><input style="height:18px; width:18px;" type='radio' name='jawaban' id="jawaban_b"
                        value='<?= $data_pertanyaan['kolom_b'] ?>' /></p>
                  </td>
                  <td scope="row">C
                    <p><input style="height:18px; width:18px;" type='radio' name='jawaban' id="jawaban_c"
                        value='<?= $data_pertanyaan['kolom_c'] ?>' /></p>
                  </td>
                  <td scope="row">D
                    <p><input style="height:18px; width:18px;" type='radio' name='jawaban' id="jawaban_d"
                        value='<?= $data_pertanyaan['kolom_d'] ?>' /></p>
                  </td>
                  <td scope="row">E
                    <p><input style="height:18px; width:18px;" type='radio' name='jawaban' id="jawaban_e"
                        value='<?= $data_pertanyaan['kolom_e'] ?>' /></p>
                  </td>

                </tr>

              </tbody>
            </table>
          </ul>
          <input type='hidden' name='nilai' id="nilai" value='<?php if (isset($nilai)) echo $nilai;
																		else echo '0'; ?>' />
          <button type="button" id="button_1" onclick="prosesNext('<?= $data_pertanyaan['id_paket_soal'] ?>');"
            class="btn btn-success">Next</button>
        </div>

      </div>
    </div>
    <br>
    <br>
</section><!-- End Hero Section -->