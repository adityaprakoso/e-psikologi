<!-- end page title end breadcrumb -->
<section @vue:mounted="mounted" id="" class="about-us padd-section">
  <div v-if="pertanyaan.length >= 1" class="container" data-aos="fade-up">
    <div class="card-deck">
      <div class="card text-left">
        <img class="card-img-top" src="holder.js/100px180/" alt="">
        <div class="card-body">
          <div class="row">
            <div class="border-dark col-md-8 pt-1 pb-5">

              <h4 class="card-title">Berikut Soal Kepribadian</h4>
              <h4 class="card-title">Soal No {{ pertanyaan[selectedQuestionIndex].nomor }}</h4>
              <p>
              <div v-if="pertanyaan[selectedQuestionIndex].gambar.length >= 1">
                <template v-for="(gambar, i) in pertanyaan[selectedQuestionIndex].gambar">
                  <img width="200px" :id="`gambar-${i}`" :src="`${baseUrl}${gambar}`" alt="gambar-soal">
                </template>
              </div>
              </p>

              <p class="lead" style="line-height: 1.5em;">{{ pertanyaan[selectedQuestionIndex].soal }}</p>
              <hr class="my-4">
              <div v-for="(data, i) in pertanyaan[selectedQuestionIndex].opsi" class="form-check">
                <input @change="saveAnswer" class="form-check-input" type="radio" name="exampleRadios"
                  :id="`exampleRadios-${i}`" :value="i" v-model="pertanyaan[selectedQuestionIndex].jawabanDipilih">
                <label style="cursor: pointer;" class="form-check-label" :for="`exampleRadios-${i}`">
                  <span style="text-transform: capitalize;">{{ (i+10).toString(36) }}.</span> {{ data }}
                </label>
              </div>
            </div>
            <div class="border-dark col-md-4 pt-1 pb-5">
              <div class="card" style="text-align: center;
    margin-bottom: 13px;">
                <h4 style="font-size: 24px;margin-top: 13px;" id="jam" class="card-title">{{ countdown }} </h4>
              </div>
              <div class="card">
                <button @click="selesai" style="width: fit-content;margin: 11px auto 0;" class="btn btm-sm btn-danger"
                  type="button">Selesai Test</button>

                <nav aria-label="..." style="margin: 0 12px;">
                  <ul class="pagination pagination-sm">
                    <!-- <template v-for="i in 50">
               <li class="page-item"aria-current="page">
                  <span class="page-link">{{ i }}</span>
               </li>
            </template>   -->
                    <template v-for="(pageNumber, index) in pertanyaan.length">
                      <li class="page-item" :class="{
                  'active': index === selectedQuestionIndex
                  }" aria-current="page">
                        <span :class="{
                     'already-question' : pertanyaan[index].jawabanDipilih !== '-'
                     }" v-if="index === selectedQuestionIndex" class="page-link">{{ pageNumber }}</span>
                        <a :class="{
                     'already-question' : pertanyaan[index].jawabanDipilih !== '-'
                     }" v-else class="page-link" @click.prevent="goTo(index)" href="#">{{ pageNumber }}</a>
                      </li>
                    </template>
                  </ul>
                </nav>
                <div style="margin-bottom: 15px;" class="text-center">
                  <a v-if="selectedQuestionIndex >= 1" @click.prevent="goPrev" type="button"
                    class="btn-get-started scrollto" href="#">
                    << KEMBALI </a>
                      <span
                        v-if="selectedQuestionIndex >= 1 && selectedQuestionIndex !== pertanyaan.length - 1">|</span>
                      <a v-if="selectedQuestionIndex !== pertanyaan.length - 1" @click.prevent="goNext" type="button"
                        class="btn-get-started scrollto " href="#">
                        SELANJUTNYA >>
                      </a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

</section>
<!-- End About Us Section -->
<style>
.pagination {
  margin: 27px auto 11px;
  width: fit-content;
  display: flex;
  flex-wrap: wrap;
}

.pagination>li {
  margin-bottom: 6px;
}

.already-question {
  background-color: #3ea73e;
  color: #ffff;
}
</style>
<script src="<?= base_url() ?>assets-admin/assets/js/petite-vue.iife.js"></script>
<script>
PetiteVue.createApp({
  baseUrl: '<?= base_url('assets-public/gambar-soal/') ?>',
  questionLength: 0,
  selectedQuestionIndex: 0,
  pertanyaan: [],
  countdown: '0',
  id_user: <?= $user['id_login']; ?>,
  // pertanyaan: [
  //  {
  //   nomor: 1,
  //   soal: 'soal 1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
  //   gambar: ['https://asia.olympus-imaging.com/content/000107507.jpg', 'https://asia.olympus-imaging.com/content/000107507.jpg', 'https://asia.olympus-imaging.com/content/000107507.jpg'],
  //   opsi : ['A', 'B', 'C', 'D', 'E'],
  //   jawabanBenar: 'B',
  //   jawabanDipilih: '-',
  //  },
  //  {
  //   nomor: 2,
  //   soal: 'soal 2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
  //   gambar: ['https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg', 'https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg', 'https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg'],
  //   opsi : ['A', 'B', 'C', 'D', 'E'],
  //   jawabanBenar: 'B',
  //   jawabanDipilih: '-',
  //  },
  //   {
  //   nomor: 3,
  //   soal: 'soal 2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
  //   gambar: ['https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg', 'https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg', 'https://www.fujifilm.com/products/digital_cameras/x/fujifilm_x_t1/sample_images/img/index/pic_01.jpg'],
  //   opsi : ['A', 'B', 'C', 'D', 'E'],
  //   jawabanBenar: 'B',
  //   jawabanDipilih: '-',
  //  },
  // ],
  mounted() {
    this.questionLength = this.pertanyaan.length;
    this.clock();
    this.pertanyaan = localStorage.getItem("LAST_ANSWER") ? JSON.parse(localStorage.getItem("LAST_ANSWER")) : JSON
      .parse('<?= $data_pertanyaan; ?>');
  },
  goNext() {
    this.selectedQuestionIndex++;
  },
  goPrev() {
    this.selectedQuestionIndex--;
  },
  goTo(page) {
    this.selectedQuestionIndex = page;
  },
  myTimer(c) {
    return myTimer = setInterval(() => {
      --c
      var seconds = c % 60;
      var minutes = (c - seconds) / 60;
      var minutesLeft = minutes % 60;
      var hours = Math.floor(minutes / 60);
      // console.clear();
      document.getElementById("jam").innerHTML = hours + ":" + minutesLeft + ":" + seconds;
      localStorage.setItem("LAST_TIME", c);
      if (c == 0) {
        this.sendKalkulasi()
        localStorage.removeItem("LAST_TIME")
        clearInterval(myTimer);
      }
    }, 1000);
  },
  clock() {
    var c = localStorage.getItem("LAST_TIME") || <?= (int) $waktu ?>;
    this.myTimer(c);
  },

  selesai() {
    Swal.fire({
      //   title: 'Sel',
      text: 'Apakah anda yakin untuk selesai ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, selesai',
      cancelButtonText: 'Batal',
      backdrop: true,
      allowOutsideClick: false
    }).then((result) => {
      if (result.isConfirmed) {
        this.sendKalkulasi()
      }
    })
  },

  removeLocalData() {
    localStorage.removeItem("LAST_TIME")
    localStorage.removeItem("LAST_ANSWER")
  },

  sendKalkulasi() {

    Swal.fire({
      text: 'Sedang mengirim hasil test....',
      icon: 'info',
      showCancelButton: false,
      showConfirmButton: false,
      backdrop: true,
      allowOutsideClick: false,
    })

    var data = {
      id_user: this.id_user,
      pertanyaan: []
    }
    this.pertanyaan.forEach(item => {
      data.pertanyaan.push({
        id_pertanyaan: item.id_soal,
        jawaban_dipilih: (item.jawabanDipilih + 10).toString(36)
      })
    })


    fetch('<?= base_url('/C_dashboard/prosesHasilTesKepribadian') ?>', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      })
      .then(response => response.json())
      .then(data => {
        clearInterval(this.myTimer());
        this.removeLocalData();
        window.location.href = `<?= base_url('/C_dashboard/hasilTesKepribadian?id_nilai=') ?>${data.id_nilai}`;
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  },

  saveAnswer() {
    setTimeout(() => {
      localStorage.setItem("LAST_ANSWER", JSON.stringify(this.pertanyaan));
      if(!((this.selectedQuestionIndex+1) == this.pertanyaan.length)){
      this.goNext();
      }
    }, 100);
  }
}).mount()
</script>