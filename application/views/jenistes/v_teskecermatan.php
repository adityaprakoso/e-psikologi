<!-- <br>
<br>

<section id="hero">

    <div class="hero-container" data-aos="fade-in">
        <br>
        <h1>Deskripsi Tes Kecermatan</h1>
        <div class="col-lg-4">
            <div class="card m-b-30 text-white ">
                <div class="card-body">


                    <ul style="list-style-type:none;">
                        <li>Nama Tes : Tes Kecermatan</li>
                        <li>Waktu : 2 menit / kolom</li>
                        <li>Jawab soal dengan teliti</li>
                        <li>contoh soal</li>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">1</td>
                                    <td>2</td>
                                    <td>3</td>
                                </tr>

                            </tbody>
                        </table>
                        <li>Angka apa yang tidak ada pada table</li>
                        <li>
                            <table class="table table-striped table-inverse table-responsive">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>1</th>
                                        <th>2</th>

                                    </tr>
                                </thead>

                            </table>
                        </li>
                        <li>jawab</li>
                        <li>angka 3</li>
                        <li>Selamat mengerjakan</li>
                        <li>Waktu ujuan akan jalan ketika anda mulai ujian</li>
                    </ul>
                    <li><a href="<?= base_url('C_dashboard/soal') ?>" class="btn-get-started scrollto">Mulai Ujian</a>
                    </li>
                </div>

            </div>

        </div>
    </div>
    <br>
    <br>
</section>End Hero Section -->

<section @vue:mounted="mounted" id="hero">
  <div class="hero-container" data-aos="fade-in">
    <br><br><br>
    <div class="col-lg-4">
      <div class="card m-b-30">
        <div class="card-header">
          <h6>Deskripsi Soal</h6>
        </div>
        <div v-if="isKuisStart" class="col-lg-12">
          <div class="card m-b-30 text-white ">
            <div class="card-body">
              <h4 style="font-size: 24px;margin-top: 13px;" id="jam" class="card-title">{{ countdown }} </h4>
              <ul style="list-style-type:none;padding:0;">
                <li id="kolom_pertanyaan">Kolom {{ parseInt(kolomSet) + 1 }}</li>
                <table class="table">
                  <thead>
                    <tr>
                      <th>A</th>
                      <th>B</th>
                      <th>C</th>
                      <th>D</th>
                      <th>E</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td v-for="(item, index) in 5" :key="index" :id="`kolom_${isiKolom(index)}`">
                        {{ isiKolom(index)  }}
                      </td>

                    </tr>

                  </tbody>
                </table>

              </ul>

            </div>

          </div>
          <br>
          <div class="card m-b-30 text-white ">
            <div class="card-body">
              <ul style="list-style-type:none;padding:0;">
                <li id="nomor_soal" value="0">soal {{ soalSet }}</li>
                <table class="table">
                  <thead>
                    <tr>
                      <th v-for="(item, index) in 4" :key="index" :id="`data_${isiSoal(index)}`">{{ isiSoal(index) }}
                      </th>
                    </tr>
                  </thead>

                </table>
                <table class="table">

                  <tbody>
                    <tr>
                      <td v-for="(item, index) in 5" :key="index" scope="row">{{ (index + 10).toString(36) }}
                        <p><input @change="jawab(isiKolom(index))" style="height:18px; width:18px;" type='radio'
                            :name='`jawaban_${soalSet}`' :id="`jawaban_${soalSet}_${index}`" :value="index"
                            v-model="jawaban" />
                        </p>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </ul>

              <!-- <buttjawabanon type="button" id="button_1" class="btn btn-success">Next</buttjawabanon> -->
            </div>

          </div>
        </div>
        <div v-else-if="!isKuisStart" class="card-body">
          <blockquote class="card-bodyquote">
            <p>Nama Tes : Tes kecermatan</p>
            Waktu pengerjaan : 2 menit / Kolom
            <hr>Contoh Soal</p>
            <ul style="list-style-type:none;padding:0;">

              <table class="table">
                <thead>
                  <tr>
                    <th>A</th>
                    <th>B</th>
                    <th>C</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td scope="row">1</td>
                    <td>2</td>
                    <td>3</td>
                  </tr>

                </tbody>
              </table>
              <li>Angka apa yang tidak ada pada table</li>
              <li>
                <table class="table table-striped table-inverse table-responsive">
                  <thead class="thead-inverse">
                    <tr>
                      <th>1</th>
                      <th>2</th>

                    </tr>
                  </thead>

                </table>
              </li>
              <p>jawabannya adalah 3</p>
              <p>Selamat mengerjakan dan tetap teliti dan fokus</p>
            </ul>
            <li><button type="button" @click="start" class="btn-get-started scrollto btn-sm">Mulai
                Ujian</button>
            </li>
          </blockquote>
        </div>

      </div>
    </div>
  </div>
</section>

<script src="<?= base_url() ?>assets-admin/assets/js/petite-vue.iife.js"></script>
<script>
PetiteVue.createApp({
  isKuisStart: false,
  kuis: [],
  timer: 0,
  kolomSet: 0,
  soalSet: 1,
  kolomLength: 0,
  kolomMapping: [],
  jawaban: '',
  sendHasil() {
    fetch('<?= base_url('/C_dashboard/prosesHasilTesKecermatan') ?>', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.kuis),
      })
      .then(response => response.json())
      .then(data => {
        clearInterval(this.myTimer());
        // this.removeLocalData();
        window.location.href = `<?= base_url('/C_dashboard/hasilTesKecermatan?id_nilai=') ?>${data.id_nilai}`;
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  },
  start() {
    this.isKuisStart = true
    this.clock();
  },
  deleteAllCheckpoint() {
    localStorage.removeItem("LAST_KOLOM")
    localStorage.removeItem("LAST_SOAL")
    localStorage.removeItem("LAST_TIME")

  },
  clearAllInterval() {
    for (var i = 1; i < 99999; i++)
      window.clearInterval(i);
  },
  myTimer(c) {
    return myTimer = setInterval(() => {
      --c
      var seconds = c % 60;
      var minutes = (c - seconds) / 60;
      var minutesLeft = minutes % 60;
      var hours = Math.floor(minutes / 60);
      // console.clear();
      document.getElementById("jam").innerHTML = hours + ":" + minutesLeft + ":" + seconds;
      localStorage.setItem("LAST_TIME", c);
      if (c == 0) {
        if (this.kolomMapping.length === this.kolomSet + 1) {
          this.clearAllInterval()
          // localStorage.removeItem("LAST_TIME")
          this.deleteAllCheckpoint();
          clearInterval(myTimer);
          alert('kuis selesai');
          this.sendHasil()
        } else {
          this.kolomSet++;
          this.soalSet = 1;
          this.clearAllInterval()
          this.clock(true);
        }
      }
    }, 1000);
  },
  clock(isSetNew = false) {
    const timerDefault = this.timer;
    if (isSetNew) {
      var c = timerDefault;

    } else {
      var c = localStorage.getItem("LAST_TIME") || timerDefault;
      //   var c = 10;

    }
    this.myTimer(c);
  },

  jawab(value) {
    setTimeout(() => {
      this.jawaban = '';
      this.kuis[this.kolomSet][this.soalSet].jawaban = value;
      if (this.kolomMapping[this.kolomSet].soalLength === this.soalSet) {
        if (this.kolomMapping.length === this.kolomSet + 1) {
          alert('kuis selesai');
          this.clearAllInterval()
          this.deleteAllCheckpoint();
          this.sendHasil()
        } else {
          this.kolomSet++;
          this.soalSet = 1;
          this.clearAllInterval()
          this.clock(true);
          localStorage.setItem("LAST_KOLOM", this.kolomSet)
          localStorage.setItem("LAST_SOAL", this.soalSet)

        }
      } else {
        this.soalSet++;
        localStorage.setItem("LAST_KOLOM", this.kolomSet)
        localStorage.setItem("LAST_SOAL", this.soalSet)
      }

    }, 200)

  },
  isiKolom(index) {
    return this.kuis[this.kolomSet][this.soalSet].isi_kolom.split(',')[index];
  },
  isiSoal(index) {
    return this.kuis[this.kolomSet][this.soalSet].soal.split(',')[index];
  },
  mounted() {

    const parseDataKuis = JSON
      .parse('<?= $data_kolom; ?>', true)
    this.kuis = Object.values(parseDataKuis.data);
    this.timer = parseDataKuis.waktu;

    this.kolomLength = parseDataKuis.data.length;

    this.kuis.forEach((item, index) => {
      this.kolomMapping.push({
        label: (index + 10).toString(36),
        soalLength: Object.values(item).length
      })
    })

    if (localStorage.getItem("LAST_KOLOM") && localStorage.getItem("LAST_SOAL")) {
      this.kolomSet = localStorage.getItem("LAST_KOLOM") || 0;
      this.soalSet = localStorage.getItem("LAST_SOAL") || 1;
      this.clock();
    }
  },






}).mount()
</script>