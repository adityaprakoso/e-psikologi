<br>
<br>

<section id="hero">
    <div class="hero-container" data-aos="fade-in">
        <br>
        <h1>Hasil Tes</h1>
        <div class="col-lg-4">
            <div class="card m-b-30 text-white ">
                <div class="card-body">
                <ul style="list-style-type:none;">
                        <li>Nama Tes : Kepribadian</li>
                        <li>Waktu : 2 Jam</li>
                        <li>Jumlah Soal :</li>
                        <li>Poin diperoleh:</li>
                        <li><b> <?= $nilai['total'] ?> </b></li>
                        <li>(Benar : <?= $nilai['jumlah_benar'] ?>, Salah : <?= $nilai['jumlah_salah'] ?>)</li>
                        <li>Selamat telah menyelesaikan</li>
                    </ul>
                    <li><a href="<?=base_url('C_dashboard/jenistes')?>" class="btn-get-started scrollto">Kembali Ujian Lain</a>
                    </li>
                </div>

            </div>

        </div>
    </div>
    <br>
    <br>
</section><!-- End Hero Section -->