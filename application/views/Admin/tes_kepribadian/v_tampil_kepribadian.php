<div class="wrapper">
     <div class="container-fluid">
         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>
                         </ol>
                     </div>
                 </div>
             </div>
         </div>

         <a type="button" class="btn btn-default btn-md " href="<?= base_url('c_admin/Paketsoal')?>">
             <i class="fa fa-undo">Kembali</i>
         </a>
         <button type="button" class="btn btn-primary btn-md " data-toggle="modal" data-target="#modelId">
             <i class="fa fa-plus"> Tambah Tes Kepribadian</i>
         </button>
         <br>
         <br>

         <!-- Modal -->
         <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title">Tambah Tes Kepribadian</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                        <div class="container-fluid">
                            <form action="<?=base_url('c_admin/add_paket_soal')?>" method="post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nama_tes" name="id_paket_soal">
                                    </div>
                                    <div class="form-group">
                                        <label for="deskripsi">Bobot</label>
                                        <input type="text" class="form-control" id="bobot" placeholder="Masukan Bobot"
                                            name="bobot">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Gambar</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Gambar"
                                            name="gambar">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Soal</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Soal"
                                            name="soal">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan a</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan a"
                                            name="op_a">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan b</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan b"
                                            name="op_b">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan c</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan c"
                                            name="op_c">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan d</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan d"
                                            name="op_d">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan e</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan e"
                                            name="op_e">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                            </form>
                        </div>
                     </div>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <h4 class="mt-0 header-title">Data Rekap Tes Kepribadian</h4>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="tech-companies-1" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th data-priority="1">Nama Tes</th>
                                             <th data-priority="3">Deskripsi</th>
                                             <th data-priority="1">Pertanyaan</th>
                                             <th data-priority="3">Bank Soal</th>
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div> <!-- end col -->
         </div> <!-- end row -->
     </div> <!-- end container -->
 </div>
 <!-- end wrapper -->

 <!-- Data Modal -->