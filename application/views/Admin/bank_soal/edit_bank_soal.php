<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>
                         </ol>
                     </div>

                 </div>
             </div>
         </div>

    <div class="section section-presentation">
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="description">
                      <h3 class="card-title font-20 mt-0">Edit Bank Soal</h3>
                      <form role="form" action="<?= base_url('c_admin/edit_proses_bank_soal') ?>" method="post">
                        <?php foreach($edit_bank_soal as $ambil_id) : ?>
                         <div class="form-group">
                              <input type="hidden" name="id_bank_soal" value="<?=$ambil_id->id_bank_soal?>" class="form-control" readonly>
                          </div>
                          <div class="form-group">
                            <label for="data_1">Soal 1</label>
                            <input type="text" class="form-control" id="data_1" placeholder="masukan nama tes"
                                name="data_1" value="<?=$ambil_id->data_1?>" >
                        </div>

                        <div class="form-group">
                            <label for="data_2">Soal 2</label>
                            <input type="text" class="form-control" id="data_2" placeholder="masukan nama tes"
                                name="data_2" value="<?=$ambil_id->data_2?>" >
                        </div>

                        <div class="form-group">
                            <label for="data_3">Soal 3</label>
                            <input type="text" class="form-control" id="data_3" placeholder="masukan nama tes"
                                name="data_3" value="<?=$ambil_id->data_3?>" >
                        </div>

                        <div class="form-group">
                            <label for="data_4">Soal 4</label>
                            <input type="text" class="form-control" id="data_4" placeholder="masukan nama tes"
                                name="data_4" value="<?=$ambil_id->data_4?>" >
                        </div>

                        <div class="form-group">
                            <label for="jawab_benar">Jawab Benar</label>
                            <input type="text" class="form-control" id="jawab_benar" placeholder="masukan nama tes"
                                name="jawab_benar" value="<?=$ambil_id->jawab_benar?>">
                        </div>
                        <?php endforeach ?>
                          <button type="submit" class="btn btn-primary mb-2">Update</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
    </div>
</div>