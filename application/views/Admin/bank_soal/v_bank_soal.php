<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>

                         </ol>
                     </div>

                 </div>
             </div>
         </div>
         <br>
         <br>
         <!-- end page title end breadcrumb -->
         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <h4 class="mt-0 header-title">Data Bank Soal</h4>
                         <p class="text-muted m-b-30 font-14">Data Bank Soal</p>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="datatable" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th data-priority="1">No</th>
                                             <th data-priority="1">Kolom</th>
                                             <th data-priority="3">Data 1</th>
                                             <th data-priority="3">Data 2</th>
                                             <th data-priority="3">Data 3</th>
                                             <th data-priority="3">Data 4</th>
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                        <?php 
                                            $no = 1;
                                            foreach($get_bank_soal as $get_pertanyaan) :
                                        ?>
                                         <tr>
                                             <td><?= $no++;?></td>
                                             <td><?= $get_pertanyaan->deskripsi_pertanyaan?></td>
                                             <td><?= $get_pertanyaan->data_1?></td>
                                             <td><?= $get_pertanyaan->data_2?></td>
                                             <td><?= $get_pertanyaan->data_3?></td>
                                             <td><?= $get_pertanyaan->data_4?></td>
                                             <td>
                                                <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/edit_bank_soal/'),$get_pertanyaan->id_bank_soal; ?>">Edit</a>
                                                <a class="btn btn-danger btn-sm" href="<?= base_url('c_admin/hapus_bank_soal/'),$get_pertanyaan->id_bank_soal; ?>" >Hapus</a>
                                             </td>
                                         </tr>
                                     <?php endforeach ?>
                                     </tbody>

                                 </table>
                             </div>

                         </div>

                     </div>
                 </div>
             </div> <!-- end col -->
         </div> <!-- end row -->

     </div> <!-- end container -->
 </div>
 <!-- end wrapper -->

 <!-- Data Modal -->