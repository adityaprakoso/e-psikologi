<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>
                         </ol>
                     </div>

                 </div>
             </div>
         </div>

    <div class="section section-presentation">
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="description">
                      <h3 class="card-title font-20 mt-0">Edit Pertanyaan</h3>
                      <form role="form" action="<?= base_url('c_admin/edit_proses_pertanyaan') ?>" method="post">
                      <?php foreach($edit_pertanyaan as $ambil_id) : ?>
                          <div class="form-group">
                              <input type="text" name="id_pertanyaan" value="<?=$ambil_id->id_pertanyaan?>" class="form-control" readonly>
                          </div>

                          <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <input type="text" name="deskripsi_pertanyaan" value="<?=$ambil_id->deskripsi_pertanyaan?>" class="form-control">
                          </div>

                        <div class="form-group">
                            <label for="kolom_a">Pertanyaan A</label>
                            <input type="text" class="form-control" id="kolom_a" placeholder="masukan Pertanyaan A"
                                name="kolom_a"  value="<?=$ambil_id->kolom_a?>" >
                        </div>

                        <div class="form-group">
                            <label for="kolom_b">Pertanyaan B</label>
                            <input type="text" class="form-control" id="kolom_b" placeholder="masukan Pertanyaan B"
                                name="kolom_b" value="<?=$ambil_id->kolom_b?>" >
                        </div>

                        <div class="form-group">
                            <label for="kolom_c">Pertanyaan C</label>
                            <input type="text" class="form-control" id="kolom_c" placeholder="masukan Pertanyaan C"
                                name="kolom_c" value="<?=$ambil_id->kolom_c?>" >
                        </div>

                        <div class="form-group">
                            <label for="kolom_d">Pertanyaan D</label>
                            <input type="text" class="form-control" id="kolom_d" placeholder="masukan Pertanyaan D"
                                name="kolom_d" value="<?=$ambil_id->kolom_d?>" >
                        </div>

                        <div class="form-group">
                            <label for="kolom_e">Pertanyaan E</label>
                            <input type="text" class="form-control" id="kolom_e" placeholder="masukan Pertanyaan E"
                                name="kolom_e" value="<?=$ambil_id->kolom_e?>">
                        </div>

                        <div class="form-group">
                            <label for="waktu">Waktu</label>
                            <input type="text" class="form-control" id="waktu" placeholder="masukan Jawaban Benar"
                                name="waktu" value="<?=$ambil_id->waktu?>">
                        </div>

                        <?php endforeach ?>
                          <button type="submit" class="btn btn-primary mb-2">Update</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>

    </div>
</div>