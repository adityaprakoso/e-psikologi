<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>
                         </ol>
                     </div>
                    </div>
                </div>
            </div>

            <?php foreach($get_id as $ambil_id) : ?>
            <button type="button" class="btn btn-info btn-md " data-toggle="modal" data-toggle="modal" data-target="#modal_id1">
                <i class="fa fa-plus"> Tambah Pertanyaan</i>
            </button>
            <?php endforeach ?>
            <br>
            <br>

              <!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                            <div class="card-body">
                            <h4 class="mt-0 header-title">Data Tes Kecermatan</h4>
                            <p class="text-muted m-b-30 font-14">Data Rekap Tes Kecermatan</p>
                            <div class="table-rep-plugin">
                                <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="datatable" class="table  table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th data-priority="1">Nama Tes</th>
                                                    <th data-priority="1">Kolom</th>
                                                    <th data-priority="1">A</th>
                                                    <th data-priority="1">B</th>
                                                    <th data-priority="1">C</th>
                                                    <th data-priority="1">D</th>
                                                    <th data-priority="1">E</th>
                                                    <th data-priority="1">Bank Soal</th>
                                                    <th data-priority="3">Aksi</th>
                                                </tr>
                                                <tbody>
                                                <?php 
                                                $no = 1;
                                                foreach($data_pertanyaan as $get_pertanyaan) :
                                                ?>
                                                <tr>
                                                    <td><?= $no++;?></td>
                                                    <td><?= $get_pertanyaan->nama_tes?></td>
                                                    <td><?= $get_pertanyaan->deskripsi_pertanyaan?></td>
                                                    <td><?= $get_pertanyaan->kolom_a?></td>
                                                    <td><?= $get_pertanyaan->kolom_b?></td>
                                                    <td><?= $get_pertanyaan->kolom_c?></td>
                                                    <td><?= $get_pertanyaan->kolom_d?></td>
                                                    <td><?= $get_pertanyaan->kolom_e?></td>
                                                    <td>
                                                        <button class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal_pertanyaan<?php echo $get_pertanyaan->id_pertanyaan;?>" >Add</button>
                                                        <a class="btn btn-success btn-sm" href="<?= base_url('c_admin/tampil_bank_soal/'),$get_pertanyaan->id_pertanyaan?>">Detail</a>
                                                    </td>
                                                    <td> 
                                                        <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/edit_pertanyaan/'),$get_pertanyaan->id_pertanyaan; ?>">Edit</a>
                                                        <a class="btn btn-danger btn-sm" href="<?= base_url('c_admin/hapus_pertanyaan/'),$get_pertanyaan->id_pertanyaan; ?>" >Hapus</a>
                                                    </td>
                                                </tr>
                                                <?php endforeach ?>
                                                </tbody>

                                            </thead>
                                        </table>
                                </div>
                            </div>
                            </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

     </div> <!-- end container -->
 </div>
 <!-- end wrapper -->

 <!-- Data Modal untuk tambah pertanyaan -->
<div class="modal fade" id="modal_id1" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pertanyaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form action="<?= base_url('c_admin/add_proses_pertanyaan')?>" method="post">
                    
                        <div class="form-group">
                        <?php foreach($get_id as $id_ambil) : ?>
                            <input type="hidden" class="form-control" id="id_paket_soal"
                            name="id_paket_soal" value="<?=$id_ambil->id_paket_soal?>">
                            <?php endforeach ?>
                        </div>

                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <input type="text" class="form-control" id="deskripsi_pertanyaan" placeholder="Masukan Deskripsi"
                                name="deskripsi_pertanyaan">
                        </div>

                        <div class="form-group">
                            <label for="kolom_a">Pertanyaan A</label>
                            <input type="text" class="form-control" id="kolom_a" placeholder="masukan Pertanyaan A"
                                name="kolom_a">
                        </div>

                        <div class="form-group">
                            <label for="kolom_b">Pertanyaan B</label>
                            <input type="text" class="form-control" id="kolom_b" placeholder="masukan Pertanyaan B"
                                name="kolom_b">
                        </div>

                        <div class="form-group">
                            <label for="kolom_c">Pertanyaan C</label>
                            <input type="text" class="form-control" id="kolom_c" placeholder="masukan Pertanyaan C"
                                name="kolom_c">
                        </div>

                        <div class="form-group">
                            <label for="kolom_d">Pertanyaan D</label>
                            <input type="text" class="form-control" id="kolom_d" placeholder="masukan Pertanyaan D"
                                name="kolom_d">
                        </div>

                        <div class="form-group">
                            <label for="kolom_e">Pertanyaan E</label>
                            <input type="text" class="form-control" id="kolom_e" placeholder="masukan Pertanyaan E"
                                name="kolom_e">
                        </div>

                        <div class="form-group">
                            <label for="waktu">Waktu</label>
                            <input type="text" class="form-control" id="waktu" placeholder="masukan Jawaban Benar"
                                name="waktu">
                        </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- end modal tambah pertanyaan -->


 <!-- data modal add pertanyaan -->
 <?php 
    foreach($data_pertanyaan as $get_id_pertanyaan) :
?>
<div class="modal fade" id="modal_pertanyaan<?php echo $get_id_pertanyaan->id_pertanyaan;?>" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Bank Soal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="container-fluid">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Pertanyaan ( <?=$get_id_pertanyaan->deskripsi_pertanyaan?> )</h4>
                            <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                                <tr>
                                                    <th data-priority="1">(A)</th>
                                                    <th data-priority="1">(B)</th>
                                                    <th data-priority="1">(C)</th>
                                                    <th data-priority="1">(D)</th>
                                                    <th data-priority="1">(E)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?=$get_id_pertanyaan->kolom_a?></td>
                                                    <td><?=$get_id_pertanyaan->kolom_b?></td>
                                                    <td><?=$get_id_pertanyaan->kolom_c?></td>
                                                    <td><?=$get_id_pertanyaan->kolom_d?></td>
                                                    <td><?=$get_id_pertanyaan->kolom_e?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                            </div>
                        </div>
                            <hr>
                    <form action="<?=base_url('c_admin/add_proses_bank_soal')?>" method="post">
                        <input type="hidden" class="form-control" id="id_pertanyaan"
                                name="id_pertanyaan" value="<?=$get_id_pertanyaan->id_pertanyaan?>">
                        <div class="form-group">
                            <label for="data_1">Soal 1</label>
                            <input type="text" class="form-control" id="data_1" placeholder="masukan nama tes"
                                name="data_1">
                        </div>

                        <div class="form-group">
                            <label for="data_2">Soal 2</label>
                            <input type="text" class="form-control" id="data_2" placeholder="masukan nama tes"
                                name="data_2">
                        </div>

                        <div class="form-group">
                            <label for="data_3">Soal 3</label>
                            <input type="text" class="form-control" id="data_3" placeholder="masukan nama tes"
                                name="data_3">
                        </div>

                        <div class="form-group">
                            <label for="data_4">Soal 4</label>
                            <input type="text" class="form-control" id="data_4" placeholder="masukan nama tes"
                                name="data_4">
                        </div>

                        <div class="form-group">
                            <label for="jawab_benar">Jawab Benar</label>
                            <input type="text" class="form-control" id="jawab_benar" placeholder="masukan nama tes"
                                name="jawab_benar">
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
<!-- end modal add pertanyaan -->
