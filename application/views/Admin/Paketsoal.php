 <div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>

                         </ol>
                     </div>

                 </div>
             </div>
         </div>

         <button type="button" class="btn btn-primary btn-md " data-toggle="modal" data-target="#modelId">
             <i class="fa fa-plus"> Tambah Paket Soal</i>
         </button>
         <br>
         <br>

         <!-- Modal -->
         <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title">Tambah paket soal</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                        <div class="container-fluid">
                            <form action="<?=base_url('c_admin/add_paket_soal')?>" method="post">
                                    <div class="form-group">
                                        <label for="nama_tes">Nama Tes</label>
                                        <input type="text" class="form-control" id="nama_tes" placeholder="masukan nama tes"
                                            name="nama_tes">
                                    </div>
                                    <div class="form-group">
                                        <label for="deskripsi">Jenis Tes</label>
                                        <select name="jenis_tes" id="" class="form-control">
                                            <option value="">- Pilih -</option>
                                            <option value="1">- Tes Kecermatan -</option>
                                            <option value="2">- Tes Kepribadian -</option>
                                            <option value="3">- Tes Kecerdasan -</option>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                            </form>
                        </div>
                     </div>
                 </div>
             </div>
         </div>



         <!-- end page title end breadcrumb -->
         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <h4 class="mt-0 header-title">Data Tes Kecermatan</h4>
                         <p class="text-muted m-b-30 font-14">Data Rekap Tes Kecermatan</p>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="datatable" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th data-priority="1">Nama Tes</th>
                                             <th data-priority="1">Pertanyaan</th>
                                             <!-- <th data-priority="3">Bank Soal</th> -->
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                        <?php 
                                            $no = 1;
                                            foreach($get_paket_soal as $get_paket) :
                                        ?>
                                            <tr>
                                                <td><?= $no++;?></td>
                                                    <td><?= $get_paket->nama_tes?></td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/tampil_pertanyaan/'),$get_paket->id_paket_soal?>">Add</a>
                                                </td>

                                                <td>
                                                    <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/edit_paket_soal/'), $get_paket->id_paket_soal?> ">Edit</a>
                                                    <a class="btn btn-danger btn-sm" href="<?= base_url('c_admin/hapus_paket_soal/'), $get_paket->id_paket_soal?> ">Hapus</a>
                                                </td>
                                            </tr>
                                     <?php endforeach ?>
                                     </tbody>
                                 </table>
                             </div>

                         </div>

                     </div>
                 </div>
             </div> <!-- end col -->
         </div> <!-- end row -->
                                            <!-- End untuk tes kecermatan -->

        <!-- Modal Edit Paket soal -->
       
        <!-- End modal paket Soal -->

         <!-- Modal -->
         <!-- <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                 <h4 class="mt-0 header-title">Berikut ini Tabel Tes Kepribadian</h4>
                 </div>
             </div>
         </div> -->



         <!-- end page title end breadcrumb 
         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <p class="text-muted m-b-30 font-14">Data Rekap Tes Kepribadian</p>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="tech-companies-1" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th data-priority="1">Nama Tes</th>
                                             <th data-priority="3">Tambah Soal</th>
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                        <?php 
                                            $no = 1;
                                            foreach($get_paket_soal as $get_paket) :
                                        ?>
                                     <tbody>
                                         <tr>
                                             <td><?= $no++;?></td>
                                             <td><?= $get_paket->nama_tes?></td>
                                             <td>
                                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_edit<?php echo $get_paket->id_paket_soal;?>">Add</button>
                                             <td>
                                                <button class="btn btn-info btn-sm">Edit</button>
                                                <button class="btn btn-danger btn-sm">Hapus</button>
                                             </td>
                                         </tr>
                                     </tbody>
                                     <?php endforeach ?>

                                 </table>
                             </div>

                         </div>

                     </div>
                 </div>
             </div> 
         </div> end row -->



         <!-- <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                 <h4 class="mt-0 header-title">Berikut ini Tabel Tes Kecerdasan</h4>
                 </div>
             </div>
         </div> -->



         <!-- end page title end breadcrumb 
         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <p class="text-muted m-b-30 font-14">Data Rekap Tes Kecerdasan</p>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="tech-companies-1" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th data-priority="1">Nama Tes</th>
                                             <th data-priority="3">Tambah Soal</th>
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                        <?php 
                                            $no = 1;
                                            foreach($get_paket_soal as $get_paket) :
                                        ?>
                                     <tbody>
                                         <tr>
                                             <td><?= $no++;?></td>
                                             <td><?= $get_paket->nama_tes?></td>
                                             <td>
                                                <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/tampil_pertanyaan/'),$get_paket->id_paket_soal?>">Add</a>
                                             </td>

                                             <td>
                                                <button class="btn btn-info btn-sm">Edit</button>
                                                <button class="btn btn-danger btn-sm">Hapus</button>
                                             </td>
                                         </tr>
                                     </tbody>
                                     <?php endforeach ?>

                                 </table>
                             </div>

                         </div>

                     </div>
                 </div>
             </div> 
         </div> end row -->


     </div> <!-- end container -->
 </div>
 <!-- end wrapper -->

 <!-- Data Modal -->


 <!-- untuk modal -->
 <?php 
    $no = 1;
    foreach($get_paket_soal as $get_paket) :
?>
 <div class="modal fade" id="modal_edit<?php echo $get_paket->id_paket_soal;?>" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title">Tambah Tes Kepribadian</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                        <div class="container-fluid">
                            <form action="<?=base_url('c_admin/proses_add_kepribadian')?>" method="post">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" value="<?php echo $get_paket->id_paket_soal;?>" id="id_paket_soal" name="id_paket_soal">
                                    </div>
                                    <div class="form-group">
                                        <label for="deskripsi">Bobot</label>
                                        <input type="text" class="form-control" id="bobot" placeholder="Masukan Bobot"
                                            name="bobot">
                                    </div>

                                    <!-- <div class="form-group">
                                        <label for="deskripsi">Gambar</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Gambar"
                                            name="gambar">
                                    </div> -->

                                    <div class="form-group">
                                        <label for="deskripsi">Soal</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Soal"
                                            name="soal">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan a</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan a"
                                            name="op_a">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan b</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan b"
                                            name="op_b">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan c</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan c"
                                            name="op_c">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan d</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan d"
                                            name="op_d">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Pilihan e</label>
                                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Pilihan e"
                                            name="op_e">
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Jawaban</label>
                                        <input type="text" class="form-control" id="jawab" placeholder="Masukan Jawab"
                                            name="jawab">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                            </form>
                        </div>
                     </div>
                 </div>
             </div>
         </div>  
         <?php endforeach ?>