<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>
                         </ol>
                     </div>

                 </div>
             </div>
         </div>

    <div class="section section-presentation">
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="description">
                      <h3 class="card-title font-20 mt-0">Edit Data Paket Soal</h3>
                      <form role="form" action="<?= base_url('c_admin/prose_edit_paket_soal') ?>" method="post">
                          <?php foreach($get_id as $ambil_id) : ?>
                          <div class="form-group">
                              <input type="hidden" name='id_paket_soal' class="form-control"  value="<?=$ambil_id->id_paket_soal?>" readonly>
                          </div>
                          <div class="form-group">
                              <label for="nama_tes">Nama Tes</label>
                              <input type="text" class="form-control" id="nama_tes" value="<?=$ambil_id->nama_tes?>"
                                  name="nama_tes">
                          </div>

                          <div class="form-group">
                              <label for="jenis_tes">Jenis Tes</label>
                                <select name="jenis_tes" class="form-control">
                                <option <?php if( $ambil_id->jenis_tes=='1'){echo "selected"; } ?> value='1'>Tes Kepribadian</option>
                                <option <?php if( $ambil_id->jenis_tes=='2'){echo "selected"; } ?> value='2'>Tes Kecermatan</option>
                                <option <?php if( $ambil_id->jenis_tes=='3'){echo "selected"; } ?> value='2'>Tes kecerdasan</option>
        
                                </select>
                          </div>
                        <?php endforeach ?>
                          <button type="submit" class="btn btn-primary mb-2">Update</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>

    </div>
</div>