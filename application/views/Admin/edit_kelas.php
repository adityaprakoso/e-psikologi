<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Edit</a></li>
                         </ol>
                     </div>

                 </div>
             </div>
         </div>

    <div class="section section-presentation">
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="description">
                      <h3 class="card-title font-20 mt-0">Edit Kelas</h3>
                      <form role="form" action="<?= base_url('c_admin/prose_edit_kelas') ?>" method="post">
                        <?php foreach($edit_kelas as $ambil_id) : ?>
                          <div class="form-group">
                              <input type="hidden" name='id_kelas' class="form-control" value="<?=$ambil_id->id_kelas?>"  readonly>
                          </div>

                                <div class="form-group">
                                     <label for="kelas">Kelas</label>
                                     <input type="text" class="form-control" id="kelas" placeholder="masukan Deskripsi"
                                         name="kelas" value="<?=$ambil_id->kelas?>">
                                 </div>

                                 <div class="form-group">
                                     <label for="deskripsi">Deskripsi</label>
                                     <input type="text" class="form-control" id="deskripsi" placeholder="masukan Deskripsi"
                                         name="deskripsi" value="<?=$ambil_id->deskripsi?>" >
                                 </div>

                                 <div class="form-group">
                                     <label for="code">Kode Kelas</label>
                                     <input type="text" class="form-control" id="code" placeholder="masukan Kode Kelas"
                                         name="code" value="<?=$ambil_id->code?>" >
                                 </div>

                                 <div class="form-group">
                                     <label for="tgl_mulai">Tanggal Mulai</label>
                                     <input type="date" class="form-control" id="tgl_mulai" placeholder="masukan tanggal mulai"
                                         name="tgl_mulai" value="<?=$ambil_id->tgl_mulai?>" >
                                 </div>

                                 <div class="form-group">
                                     <label for="tgl_selesai">Tanggal Selesai</label>
                                     <input type="date" class="form-control" id="tgl_selesai" placeholder="masukan Pertanyaan D"
                                         name="tgl_selesai" value="<?=$ambil_id->tgl_selesai?>" >
                                 </div>

                          <?php endforeach ?>
                          <button type="submit" class="btn btn-primary mb-2">Update</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>

    </div>
</div>