<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="itxVVxAPwUlBx98YzEn2Zmd1meqoFcAOkyaUrDxC">

    <title>Siswa - Beranda &bull; KIP Kuliah</title>

    <link rel="apple-touch-icon" sizes="180x180"
        href="https://kip-kuliah.kemdikbud.go.id/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://kip-kuliah.kemdikbud.go.id/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://kip-kuliah.kemdikbud.go.id/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://kip-kuliah.kemdikbud.go.id/img/favicon/site.webmanifest">
    <link rel="mask-icon"
        href="https://kip-kuliah.kemdikbud.go.id/img/favicon/safari-pinned-tab.svg&quot; color=&quot;#5bbad5">
    <link rel="shortcut icon" href="https://kip-kuliah.kemdikbud.go.id/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#2786fb">

    <link rel="stylesheet" href="https://kip-kuliah.kemdikbud.go.id/css/app.css?v=1.2.2 ">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" />
    <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" type="text/css"
        href="//cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/r-2.2.2/datatables.min.css" />

    <style>
    .error {
        color: firebrick;
    }

    .select2-selection--single {
        height: 39px !important;
    }

    .select2-selection__rendered {
        line-height: 28px !important;
    }

    .select2-selection__arrow {
        height: 38px !important;
    }

    .form-group.required>label:after {
        content: " *";
        color: #e3342f;
    }

    .form-group.required>label.title:after {
        content: " *";
        color: #e3342f;
    }
    </style>

    <style>
    .btn-header {
        width: 10.5%;
        min-width: 50px;
        margin-top: 10px;
    }

    .nav {
        flex-wrap: inherit;
    }

    .nav-tabs {
        display: inline-flex;
        width: 100%;
        overflow-x: auto;
        overflow-y: hidden;
        background-image: linear-gradient(to right, white, white), linear-gradient(to right, white, white), linear-gradient(to right, rgba(159, 159, 159, .50), rgba(255, 255, 255, 0)), linear-gradient(to left, rgba(159, 159, 159, .50), rgba(255, 255, 255, 0));
        /* Shadows */
        /* Shadow covers */
        background-position: left center, right center, left center, right center;
        background-repeat: no-repeat;
        background-color: white;
        background-size: 20px 100%, 20px 100%, 15px 100%, 15px 100%;
        background-attachment: local, local, scroll, scroll;
    }

    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
        border-width: 0;
    }

    .nav-tabs>li>a {
        border: none;
        color: #666;
        white-space: nowrap;
    }

    .nav-tabs .nav-link.active,
    .nav-tabs>li.active>a,
    .nav-tabs>li>a:hover {
        border: none;
        color: #4285F4 !important;
        background: transparent;
    }

    .nav-tabs .nav-link.active::after,
    .nav-tabs>li>a::after {
        content: "";
        background: #4285F4;
        height: 2px;
        position: absolute;
        width: 100%;
        left: 0px;
        bottom: 1px;
        transition: all 250ms ease 0s;
        transform: scale(0);
    }

    @media(max-width:768px) {

        .nav-tabs .nav-link.active::after,
        .nav-tabs>li.active>a::after {
            transform: scale(1) !important;
        }

        .nav-tabs>li>a:hover {
            color: #666 !important;
        }
    }

    @media(min-width:768px) {

        .nav-tabs .nav-link.active::after,
        .nav-tabs>li.active>a::after,
        .nav-tabs>li:hover>a::after {
            transform: scale(1) !important;
        }
    }

    .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }

    .nav-cetak {
        color: #2786fb !important;
    }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-gradient">
            <!-- Left navbar -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="https://kip-kuliah.kemdikbud.go.id/img/user.png"
                            class="user-image img-circle bg-white elevation-1" alt="User Image">
                        <span class="d-none d-md-inline">

                        </span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <li class="user-header bg-primary">
                            <img src="https://kip-kuliah.kemdikbud.go.id/img/user.png"
                                class="img-circle bg-white elevation-2" alt="User Image">
                            <p>
                                Nur Fajriani
                                <small>1121.406.00221.1639.784</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <a href="https://kip-kuliah.kemdikbud.go.id/siswa/auth/logout"
                                class="btn btn-default btn-flat float-right"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="https://kip-kuliah.kemdikbud.go.id/siswa/auth/logout"
                                method="POST" style="display: none;">
                                <input type="hidden" name="_token" value="itxVVxAPwUlBx98YzEn2Zmd1meqoFcAOkyaUrDxC">
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!-- Main Sidebar -->
        <aside class="main-sidebar sidebar-light-secondary elevation-6">
            <a href="/" class="brand-link">
                <img src="https://kip-kuliah.kemdikbud.go.id/img/logo.png" alt="KIPK Logo"
                    class="brand-image img-circle bg-white elevation-3">
                <span class="brand-text font-weight-light">KIP Kuliah</span>
            </a>
            <div class="sidebar">
                <div class="user-panel mt-2 pb-2 mb-3 d-flex">
                    <div class="image">
                        <img src="https://kip-kuliah.kemdikbud.go.id/img/user.png"
                            class="img-circle bg-white elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block" onclick="return false;">
                            <b>Nur Fajriani</b>
                        </a>
                        <span class="badge badge-dark">1121.406.00221.1639.784</span>
                    </div>
                </div>
                <nav class="mt-2">
                    <div class="mx-2">
                        <h5 class="text-center">
                            <b>Status Akun</b><br />
                            <div class="badge badge-warning" style="white-space: normal">
                                Belum Terdata DTKS
                            </div>

                        </h5>
                    </div>
                    <hr style="border-color: #ebe9fc; border-radius:5px;">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item">
                            <a class="nav-link active" href="https://kip-kuliah.kemdikbud.go.id/siswa/home">
                                <i class="nav-icon far fa-window-maximize"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://kip-kuliah.kemdikbud.go.id/siswa/auth/logout"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="nav-icon fas fa-power-off"></i>
                                <p>Logout</p>
                            </a>
                            <form id="logout-form" action="https://kip-kuliah.kemdikbud.go.id/siswa/auth/logout"
                                method="POST" style="display: none;">
                                <input type="hidden" name="_token" value="itxVVxAPwUlBx98YzEn2Zmd1meqoFcAOkyaUrDxC">
                            </form>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>

        <!-- Content Wrapper -->
        <div class="content-wrapper">
            <div class="misc-content pt-4">
                <div class="container">
                    <div class="alert alert-warning">
                        <i class="fas fa-info-circle"></i> <b>Informasi</b><br />
                        <ul class="m-0">
                            <li>Data Terpadu Kesejahteraan Sosial (DTKS) Anda belum ditemukan di pusat pangkalan data
                                Kemensos.</li>
                            <li>Apabila sudah mengajukan DTKS, Anda dapat cek berkala dengan menekan tombol
                                <button type="button" id="sync-kemensos" class="btn btn-sm btn-success">Cek status
                                    DTKS</button>
                            </li>
                        </ul>
                    </div>
                    <div class="row justify-content-center">
                        <div class="card card-primary card-outline card-outline-tabs m-0 p-0 col-md-12">
                            <div class="card-header p-0 border-bottom-0">
                            </div>
                            <div class="card-body p-0">
                                <div class="accordion" id="accordionExample">
                                    <div class="card card-primary card-outline">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="row justify-content-between">
                                                <div class="col-8 pt-lg-2">
                                                    Status Kelengkapan Berkas
                                                    <span class="badge badge-success">Lengkap</span>

                                                </div>
                                                <div class="col-4 text-right">
                                                    <button class="btn btn-primary" type="button" data-toggle="collapse"
                                                        data-target="#collapseOne" aria-expanded="true"
                                                        aria-controls="collapseOne">
                                                        Detail <i class="fas fa-caret-down"></i>
                                                    </button>
                                                </div>
                                            </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                            data-parent="#accordionExample">
                                            <div class="card-body">
                                                <table class="table table-responsive table-borderless table-hover">
                                                    <tr style="border-bottom: 1px solid #ddd">
                                                        <th>Formulir</th>
                                                        <th>Sudah Lengkap?</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biodata</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keluarga</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ekonomi</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rumah</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Aset</td>
                                                        <td class="text-center">
                                                            <i class="far fa-times-circle" style="color:#e3342f"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Prestasi (Opsional)</td>
                                                        <td class="text-center">
                                                            <i class="far fa-times-circle" style="color:#e3342f"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rencana</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Seleksi</td>
                                                        <td class="text-center">
                                                            <i class="far fa-check-circle" style="color:#38c172"></i>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="card card-primary card-outline card-outline-tabs col-md-12">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="formulir-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format active" id="biodata-tab" data-toggle="pill"
                                            href="#biodata" role="tab" aria-controls="biodata"
                                            aria-selected="true">Biodata</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="keluarga-tab" data-toggle="pill"
                                            href="#keluarga" role="tab" aria-controls="keluarga"
                                            aria-selected="false">Keluarga</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="ekonomi-tab" data-toggle="pill"
                                            href="#ekonomi" role="tab" aria-controls="ekonomi"
                                            aria-selected="false">Ekonomi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="rumah-tab" data-toggle="pill"
                                            href="#rumah" role="tab" aria-controls="rumah"
                                            aria-selected="false">Rumah</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="aset-tab" data-toggle="pill"
                                            href="#aset" role="tab" aria-controls="aset" aria-selected="false">Aset</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="prestasi-tab" data-toggle="pill"
                                            href="#prestasi" role="tab" aria-controls="prestasi"
                                            aria-selected="false">Prestasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="rencana-tab" data-toggle="pill"
                                            href="#rencana" role="tab" aria-controls="rencana"
                                            aria-selected="false">Rencana</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format" id="seleksi-tab" data-toggle="pill"
                                            href="#seleksi" role="tab" aria-controls="seleksi"
                                            aria-selected="false">Seleksi</a>
                                    </li>


                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format nav-cetak" id=""
                                            href="https://kip-kuliah.kemdikbud.go.id/siswa/cetak/kartu/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ=="
                                            aria-selected="false"><i class="fa fa-file-download"></i> Cetak Kartu
                                            Peserta</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link remove-tab-format nav-cetak" id=""
                                            href="https://kip-kuliah.kemdikbud.go.id/siswa/cetak/formulir/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ=="
                                            aria-selected="false"><i class="fa fa-file-download"></i> Cetak Formulir</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="formulir-tabsContent">
                                    <div class="tab-pane fade show active" id="biodata" role="tabpanel"
                                        aria-labelledby="biodata-tab"></div>
                                    <div class="tab-pane fade" id="keluarga" role="tabpanel"
                                        aria-labelledby="keluarga-tab"></div>

                                    <div class="tab-pane fade" id="ekonomi" role="tabpanel"
                                        aria-labelledby="ekonomi-tab"></div>
                                    <div class="tab-pane fade" id="rumah" role="tabpanel" aria-labelledby="rumah-tab">
                                    </div>
                                    <div class="tab-pane fade" id="aset" role="tabpanel" aria-labelledby="aset-tab">
                                    </div>
                                    <div class="tab-pane fade" id="prestasi" role="tabpanel"
                                        aria-labelledby="prestasi-tab"></div>
                                    <div class="tab-pane fade" id="rencana" role="tabpanel"
                                        aria-labelledby="rencana-tab"></div>
                                    <div class="tab-pane fade" id="seleksi" role="tabpanel"
                                        aria-labelledby="seleksi-tab"></div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>

        <!-- Main Footer -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-inline">
                KIPK
            </div>
            <strong>Copyright &copy; 2021 <a href="">Kartu Indonesia Pintar Kuliah</a>.</strong>
            All rights reserved.
        </footer>
    </div>

    <script src="https://kip-kuliah.kemdikbud.go.id/js/app.js"></script>
    <script src="https://kip-kuliah.kemdikbud.go.id/js/submit-loader.js?v=1.0" defer></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <!-- DataTables -->
    <!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
    <script type="text/javascript"
        src="//cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/r-2.2.2/datatables.min.js">
    </script>
    <!-- Start of kip-kuliah Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=23d014a9-7387-45c1-9931-60202f091520">
    </script>
    <!-- End of kip-kuliah Zendesk Widget script -->

    <!-- Script Initialization -->
    <script>
    $(function() {
        // $("body").overlayScrollbars({});
        // $('[data-toggle="tooltip"]').tooltip();
        $('.select2').select2();
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
    // setDatatable("#main-table");
    function setDatatable(element) {
        var t = $(element).DataTable({
            "responsive": true,
            "dom": "tr",
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }, {
                "searchable": false,
                "orderable": false,
                "targets": "nosort"
            }],
            "order": [
                [0, 'asc']
            ]
        });

        // t.on('order.dt search.dt', function () {
        //     t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
        //         cell.innerHTML = i + 1;
        //     });
        // }).draw();
    }

    function DeleteFunction(endpoint) {
        Swal.fire({
            title: 'Konfirmasi',
            html: 'Anda yakin ingin menghapus data ini? <br/><u>Catatan: Aksi ini tidak dapat dipulihkan.</u>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Batalkan'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'info',
                    title: 'Sedang memproses...',
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                $.ajax({
                    type: "POST",
                    data: {
                        _token: "itxVVxAPwUlBx98YzEn2Zmd1meqoFcAOkyaUrDxC"
                    },
                    url: endpoint,
                    success: function() {
                        location.reload();
                    }
                });
            }
        });
    }
    </script>
    <script>
    function callSwal(type, message, reload = false) {
        let confirm_button = type == 'error' ? true : false;
        let timer = type == 'error' ? 5000 : 1500;
        Swal.fire({
            position: 'top-end',
            icon: type,
            title: message,
            showConfirmButton: confirm_button,
            timer: timer
        });
        if (reload) {
            setTimeout(function() {
                window.location.reload();
            }, timer);
        }
    }

    function getCard(idElement, url) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "html",
            beforeSend: function() {
                $('#' + idElement).html('\
                    <div class="text-center m-4">\
                        <img src="/img/loading.gif" alt="">\
                    </div>\
                ');
            },
            success: function(result) {
                $("#" + idElement).html(result);
            },
            error: function(error) {
                if (error.status == 419) {
                    window.location.reload();
                    return false;
                }

                $('#' + idElement).html(
                    '\
                    <div class="alert alert-warning text-center">\
                        <div class="pb-2"><i class="fas fa-4x fa-exclamation-circle"></i></div>\
                        <b>Gagal memuat formulir.</b><br/>Coba muat ulang formulir dengan klik tombol\
                        <button class="btn btn-sm btn-dark" onclick="getCard(\'' + idElement + '\', \'' + url + '\')">\
                            <i class="fas fa-redo"></i> Muat ulang formulir</button>\
                    </div>\
                    '
                );
            }
        });
    }


    getCard("biodata", "https://kip-kuliah.kemdikbud.go.id/siswa/siswa/M3NZQzB5NlZtMy9UcGpOQkRQU3VKYTZq");
    getCard("keluarga",
        "https://kip-kuliah.kemdikbud.go.id/siswa/beasiswa/keluarga/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");
    getCard("prestasi", "https://kip-kuliah.kemdikbud.go.id/siswa/prestasi/M3NZQzB5NlZtMy9UcGpOQkRQU3VKYTZq");
    getCard("rencana", "https://kip-kuliah.kemdikbud.go.id/siswa/rencana/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");

    getCard("ekonomi",
        "https://kip-kuliah.kemdikbud.go.id/siswa/beasiswa/ekonomi/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");
    getCard("rumah",
    "https://kip-kuliah.kemdikbud.go.id/siswa/beasiswa/rumah/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");
    getCard("aset", "https://kip-kuliah.kemdikbud.go.id/siswa/aset/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");

    getCard("seleksi", "https://kip-kuliah.kemdikbud.go.id/siswa/seleksi/MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ==");

    function copyText(targetId) {
        var text = $("#" + targetId).get(0);
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
        //add to clipboard.
        document.execCommand("copy");
        callSwal('success', 'Teks telah disalin');
    }
    </script>

    <script>
    $(document).ready(function() {
        $('#sync-kemensos').on('click', function() {
            Swal.fire({
                title: 'Konfirmasi',
                text: 'Lakukan sinkronisasi data DTKS Kemensos?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Lanjutkan',
                cancelButtonText: 'Batalkan'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'info',
                        title: 'Sedang memproses... mohon jangan menutup browser Anda.',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    $.ajax({
                        url: 'https://kip-kuliah.kemdikbud.go.id/siswa/sync-kemensos',
                        type: 'POST',
                        data: {
                            _token: 'itxVVxAPwUlBx98YzEn2Zmd1meqoFcAOkyaUrDxC',
                            id: 'MitGNHZkc2Y1R2JXNWlUeDhDRWF3UEdWRHc9PQ=='
                        },
                        success: function(d) {
                            console.log(d);
                            Swal.close();
                            let response = JSON.parse(d);
                            if (response.status)
                                callSwal('success', response.message, true);
                            else
                                callSwal('warning', response.message);
                        },
                        error: function(err) {
                            Swal.close();
                            callSwal('error',
                                'Terjadi kesalahan, mohon ulangi lagi.')
                        }
                    });
                }
            })
        });
    })
    </script>
</body>

</html>