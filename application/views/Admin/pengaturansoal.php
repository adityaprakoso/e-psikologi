<div class="wrapper">
     <div class="container-fluid">

         <!-- Page-Title -->
         <div class="row">
             <div class="col-sm-12">
                 <div class="page-title-box">
                     <div class="btn-group pull-right">
                         <ol class="breadcrumb hide-phone p-0 m-0">
                             <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                             <li class="breadcrumb-item"><a href="#">Tabel</a></li>

                         </ol>
                     </div>

                 </div>
             </div>
         </div>

         <button type="button" class="btn btn-primary btn-md " data-toggle="modal" data-target="#modelId">
             <i class="fa fa-plus"> Tambah Kelas</i>
         </button>
         <br>
         <br>

         <!-- Modal -->
         <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title">Tambah Kelas</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                         <div class="container-fluid">
                         <form action="<?= base_url('c_admin/add_proses_pengaturan')?>" method="post">
                                <select name="id_paket_soal" id="id_paket_soal" class="form-control">
                                    <option value="">- Pilih Pertanyaan -</option>
                                    <?php foreach($get_paket_soal as $tampil_pertanyaan ) : ?>
                                        <option value="<?=$tampil_pertanyaan->id_paket_soal?>"><?=$tampil_pertanyaan->nama_tes?></option>
                                    <?php endforeach ?>
                                </select>
                                
                                <div class="form-group">
                                     <label for="kelas">Kelas</label>
                                     <input type="text" class="form-control" id="kelas" placeholder="masukan Deskripsi"
                                         name="kelas">
                                 </div>

                                 <div class="form-group">
                                     <label for="deskripsi">Deskripsi</label>
                                     <input type="text" class="form-control" id="deskripsi" placeholder="masukan Deskripsi"
                                         name="deskripsi">
                                 </div>

                                 <div class="form-group">
                                     <label for="code">Kode Kelas</label>
                                     <input type="text" class="form-control" id="code" placeholder="masukan Kode Kelas"
                                         name="code">
                                 </div>

                                 <div class="form-group">
                                     <label for="tgl_mulai">Tanggal Mulai</label>
                                     <input type="date" class="form-control" id="tgl_mulai" placeholder="masukan tanggal mulai"
                                         name="tgl_mulai">
                                 </div>

                                 <div class="form-group">
                                     <label for="tgl_selesai">Tanggal Selesai</label>
                                     <input type="date" class="form-control" id="tgl_selesai" placeholder="masukan Pertanyaan D"
                                         name="tgl_selesai">
                                 </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
         </div>



         <!-- end page title end breadcrumb -->
         <div class="row">
             <div class="col-12">
                 <div class="card m-b-30">
                     <div class="card-body">
                         <h4 class="mt-0 header-title">Data Kelas Ujian</h4>
                         <p class="text-muted m-b-30 font-14">Data Rekap Kelas Ujian</p>
                         <div class="table-rep-plugin">
                             <div class="table-responsive b-0" data-pattern="priority-columns">
                                 <table id="datatable" class="table  table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th data-priority="1">Nama Tes</th>
                                             <th data-priority="3">kelas</th>
                                             <th data-priority="1">Code</th>
                                             <th data-priority="3">Tanggal Mulai</th>
                                             <th data-priority="3">Tanggal Selesai</th>
                                             <th data-priority="3">Aksi</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                        <?php 
                                            $no = 1;
                                            foreach($get_pengaturan as $get_paket) :
                                        ?>
                                         <tr>
                                             <td><?= $no++;?></td>
                                             <td><?= $get_paket->nama_tes?></td>
                                             <td><?= $get_paket->kelas?></td>
                                             <td><?= $get_paket->code?></td>
                                             <td><?= $get_paket->tgl_mulai?></td>
                                             <td><?= $get_paket->tgl_selesai?></td>

                                             <td>
                                                <a class="btn btn-info btn-sm" href="<?= base_url('c_admin/edit_kelas/'),$get_paket->id_kelas; ?>">Edit</a>
                                                <a class="btn btn-danger btn-sm" href="<?= base_url('c_admin/hapus_kelas/'),$get_paket->id_kelas; ?>" >Hapus</a>
                                             </td>
                                         </tr>
                                     <?php endforeach ?>
                                     </tbody>

                                 </table>
                             </div>

                         </div>

                     </div>
                 </div>
             </div> <!-- end col -->
         </div> <!-- end row -->

     </div> <!-- end container -->
 </div>
 <!-- end wrapper -->

 <!-- Data Modal -->