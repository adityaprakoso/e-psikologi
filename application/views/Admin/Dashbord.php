<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">E-Psikologi</a></li>
                            <li class="breadcrumb-item active">Dashbord</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="container">
            <div class="alert alert-warning">
                <i class="fas fa-info-circle"></i> <b>Informasi</b><br />
                <ul class="m-0">
                    <li>Selamat Datang Diaplikasi Ujian Psikologi
                    </li>
                    <li>Apabilah sudah melakukan input data, data tersebut akan muncul di bawa ini
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <img src="<?=base_url()?>assets-admin/assets/images/coins/btc.png" alt=""
                                    class="rounded-curcle">
                            </div>
                            <div class="col-5">
                                <h4 class="counter text-dark m-0 pb-1">PAKET SOAL</h4>
                                <i class="mdi mdi-arrow-down text-danger"></i> <small class="text-danger">Jumlah</small>
                            </div>
                            <div class="col-12">
                                <div id="sparkline0"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <img src="<?=base_url()?>assets-admin/assets/images/coins/eth.png" alt=""
                                    class="rounded-curcle">
                            </div>
                            <div class="col-4">
                                <h4 class="counter text-dark m-0 pb-1">KELAS</h4>
                                <i class="mdi mdi-arrow-up text-success"></i> <small class="text-success">Jumlah</small>
                            </div>
                            <div class="col-12">
                                <div id="sparkline1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3  align-items-center">
                    <div class="mini-stat clearfix bg-white">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <img src="<?=base_url()?>assets-admin/assets/images/coins/dash.png" alt=""
                                    class="rounded-curcle">
                            </div>
                            <div class="col-4">
                                <h4 class="counter text-dark m-0 pb-1">PESERTA</h4>
                                <i class="mdi mdi-arrow-down text-danger"></i> <small class="text-danger">Jumlah</small>
                            </div>
                            <div class="col-12">
                                <div id="sparkline2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>