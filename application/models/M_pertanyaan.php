<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pertanyaan extends CI_Model {

    // public function get_pertanyaan()
    // {
    //     $this->db->select('*');
    //     $this->db->from('tb_pertanyaan');
    //     $this->db->join('tb_paket_soal', 'tb_paket_soal.id_paket_soal = tb_pertanyaan.id_paket_soal');
    //     $query = $this->db->get();
    //     return $query;
    // }

    public function get_id()
    {
        $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        $this->db->join('tb_paket_soal', 'tb_paket_soal.id_paket_soal = tb_pertanyaan.id_paket_soal');
        $query = $this->db->get();
        return $query;
    }

    public function tampil()
    {
        $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        // $this->db->join('tb_paket_soal', 'tb_paket_soal.id_paket_soal = tb_pertanyaan.id_paket_soal');
        $query = $this->db->get();
        return $query;
    }

    public function get_where($id)
    {
        $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        $this->db->join('tb_paket_soal','tb_paket_soal.id_paket_soal = tb_pertanyaan.id_paket_soal');
        $this->db->where('tb_pertanyaan.id_paket_soal',$id);
        $query = $this->db->get();
        return $query;
    }    
    
    public function get_where_by($field=null,$value=null)
    {
        $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        $this->db->where($field,$value);
        $this->db->order_by('id_pertanyaan','ASC');
        $query = $this->db->get();
        return $query;
    }

    public function get_data_pertanyaan($field=null,$value=null, $nomor=0)
    {
        $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        $this->db->where($field,$value);
        $this->db->order_by('id_pertanyaan','ASC');
        $this->db->limit(2, $nomor);
        $query = $this->db->get();
        return $query;
    }

    public function get_jumlah_pertanyaan($field=null,$value=null)
    {
        // $this->db->select('*');
        $this->db->from('tb_pertanyaan');
        $this->db->where($field,$value);
        return $this->db->count_all_results();
    }

    public function add_model_pertanyaan($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function edit_pertanyaan($where,$table)
    {
        return $this->db->get_where($table, $where);
    }
    
    public function update_data($table,$where, $data)
    {
        $this->db->update($table,$where, $data);
    }

    public function hapus_pertanyaan($id)
    {
        $this->db->where('id_pertanyaan',$id);
        $this->db->delete('tb_pertanyaan');
        return true;
    }

}
