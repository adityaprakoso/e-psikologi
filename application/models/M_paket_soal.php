<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_paket_soal extends CI_Model {

    public function get_paket_soal()
    {
        $this->db->select('*');
        $this->db->from('tb_paket_soal');
        $query = $this->db->get();
        return $query;
    }


    public function add_model_paket_soal($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function get_id($where,$table)
    {
        return $this->db->get_where($table, $where);
    }

    // public function get_where($field=null,$value=null)
    // {
    //     $this->db->select('*');
    //     $this->db->from('tb_paket_soal');
    //     $this->db->where($field, $value);
    //     return $this->db->get();
    // }
    public function get_waktu($selected, $value){
        $this->db->select($selected.' AS waktu');
        $this->db->from('tb_kelas');
        $this->db->where('id_paket_soal', $value);
        return $this->db->get();
    }

    public function update_data($table,$where, $data)
    {
        $this->db->update($table,$where, $data);
    }

    public function hapus_id_paket_soal($id_paket_soal){
        $this->db->where('id_paket_soal',$id_paket_soal);
        $this->db->delete('tb_paket_soal');
        return true;
   }
}
