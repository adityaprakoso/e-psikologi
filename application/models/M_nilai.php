<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_nilai extends CI_Model {

    public function get_nilai()
    {
        $this->db->select('*');
        $this->db->from('tb_nilai');
        $query = $this->db->get();
        return $query;
    }

    public function insert_nilai($data)
    {
        $insert = $this->db->insert('tb_nilai', $data);
        if($insert) return $this->db->insert_id();
        else return false;
    }

    public function get_id($where,$table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_where($field=null,$value=null)
    {
        $this->db->select('*');
        $this->db->from('tb_nilai');
        $this->db->where($field, $value);
        return $this->db->get();
    }

}
