<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_bank_soal extends CI_Model {

    public function get_bank_soal($id)
    {
        $this->db->select('*');
        $this->db->from('tb_bank_soal');
        $this->db->join('tb_pertanyaan', 'tb_pertanyaan.id_pertanyaan = tb_bank_soal.id_pertanyaan');
        $this->db->where('tb_bank_soal.id_pertanyaan', $id);
        $query = $this->db->get();
        return $query;
    }

    public function get_where($field=null,$value=null)
    {
        $this->db->select('*');
        $this->db->from('tb_bank_soal');
        $this->db->where($field, $value);
        $this->db->order_by('id_bank_soal','ASC');
        return $this->db->get();
    }

    public function get_data_soal($field=null,$value=null, $nomor=0)
    {
        $this->db->select('*');
        $this->db->from('tb_bank_soal');
        $this->db->where($field, $value);
        $this->db->order_by('id_bank_soal','ASC');
        $this->db->limit(2, $nomor);
        return $this->db->get();
    }

    public function get_jumlah_soal($field=null,$value=null)
    {
        $this->db->select('*');
        $this->db->from('tb_bank_soal');
        $this->db->where($field, $value);
        return $this->db->count_all_results();
    }
    
    public function add_model_bank_soal($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function edit_bank_soal($where,$table)
    {
        return $this->db->get_where($table, $where);
    }
    
    public function update_data($table,$where, $data)
    {
        $this->db->update($table,$where, $data);
    }

    public function hapus_bank_soal($id)
    {
        $this->db->where('id_bank_soal',$id);
        $this->db->delete('tb_bank_soal');
        return true;
    }

}
