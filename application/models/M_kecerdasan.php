<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kecerdasan extends CI_Model {

    public function get_soal($id)
    {
        $this->db->select('*');
        $this->db->from('tb_kecerdasan');
        $this->db->join('tb_paket_soal', 'tb_paket_soal.id_paket_soal = tb_kecerdasan.id_paket_soal');
        $this->db->where('tb_kecerdasan.id_paket_soal', $id);
        $query = $this->db->get();
        return $query;
    }

    public function add_data($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function get_where($field=null,$value=null)
    {
        $this->db->select('*');
        $this->db->from('tb_kecerdasan');
        $this->db->where($field, $value);
        return $this->db->get();
    }
}