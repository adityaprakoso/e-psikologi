<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengaturan extends CI_Model {

    public function get_pengaturan()
    {
        $this->db->select('*');
        $this->db->from('tb_kelas');
        $this->db->join('tb_paket_soal', 'tb_paket_soal.id_paket_soal = tb_kelas.id_paket_soal');
        $query = $this->db->get();
        return $query;
    }

    public function get_kelas_by_code($kode_ujian=null)
    {
        $this->db->select('*');
        $this->db->from('tb_kelas');
        $this->db->where('code', $kode_ujian);
        return $this->db->get()->row();
        #$query = $this->db->get();
        #return $query;
    }

    public function add_model_pengaturan($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function edit_kelas($where,$table)
    {
        return $this->db->get_where($table, $where);
    }
    
    public function update_data($table,$where, $data)
    {
        $this->db->update($table,$where, $data);
    }

    public function hapus_kelas($id)
    {
        $this->db->where('id_kelas',$id);
        $this->db->delete('tb_kelas');
        return true;
    }

}
