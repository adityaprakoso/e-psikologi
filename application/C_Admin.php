<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        // check_not_login();
		$this->load->model('m_daftar');
    }

	public function index()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Dashbord');
		$this->load->view('template-admin/footer');
    }
	public function Dashbord()
	{
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Dashbord');
		$this->load->view('template-admin/footer');	
	} 
	public function Aplikasi()
	{
		$data['aplikasi'] = $this->m_daftar->ambil_aplikasi()->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Aplikasi', $data);
		$this->load->view('template-admin/footer');	
	} 
	public function Domain()
	{
		$data['domain'] = $this->m_daftar->ambil_domain()->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Domain', $data);
		$this->load->view('template-admin/footer');	
	} 
	public function Zoom(){
		$this->load->view('template-admin/header');
		$this->load->view('Admin/Zoom');
		$this->load->view('template-admin/footer');	
	} 
    
	public function verifikasi_edit($id){
		$where = array('id_aplikasi' => $id);
		$data['verifikasi'] = $this->m_daftar->verifikasi_update($where,'tb_aplikasi')->result();
		$this->load->view('template-admin/header');
		$this->load->view('Admin/verifikasi_admin', $data);
		$this->load->view('template-admin/footer');	
	}


	public function update()
	{
		$id_aplikasi 	= $this->input->post('id_aplikasi');
		$verifikasi = $this->input->post('verifikasi');

		$data = array(
			'verifikasi' => $verifikasi,
		);

		$where = array(
			'id_aplikasi' => $id_aplikasi
		);

		$this->m_daftar->update_data('tb_aplikasi', $data, $where);
		redirect('C_Admin/Aplikasi');
	}
}