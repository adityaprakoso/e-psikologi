/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 8.0.26-0ubuntu0.20.04.3 : Database - db_psikologi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tb_bank_soal` */

DROP TABLE IF EXISTS `tb_bank_soal`;

CREATE TABLE `tb_bank_soal` (
  `id_bank_soal` int NOT NULL AUTO_INCREMENT,
  `id_pertanyaan` int NOT NULL,
  `data_1` text NOT NULL,
  `data_2` text NOT NULL,
  `data_3` text NOT NULL,
  `data_4` text NOT NULL,
  `jawab_benar` text NOT NULL,
  PRIMARY KEY (`id_bank_soal`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `tb_bank_soal` */

insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (10,11,'1','3','4','5','2');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (11,11,'1','2','4','5','3');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (12,11,'2','3','4','5','1');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (13,11,'1','2','3','4','5');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (14,12,'A','B','C','D','E');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (15,13,'3','4','5','6','7');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (16,14,'b','c','d','#','()');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (17,15,'!','@','#','$','%');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (18,15,'!','%','#','$','@');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (19,16,'!','@','#','$','()');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (20,16,'!','@','()','$','#');
insert  into `tb_bank_soal`(`id_bank_soal`,`id_pertanyaan`,`data_1`,`data_2`,`data_3`,`data_4`,`jawab_benar`) values (21,16,'#','@','()','$','!');

/*Table structure for table `tb_kecerdasan` */

DROP TABLE IF EXISTS `tb_kecerdasan`;

CREATE TABLE `tb_kecerdasan` (
  `id_kecerdasan` int NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int NOT NULL,
  `bobot` int NOT NULL,
  `gambar` varchar(30) NOT NULL,
  `soal` text NOT NULL,
  `op_a` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_b` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_c` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_d` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_e` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jawab` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kecerdasan`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tb_kecerdasan` */

insert  into `tb_kecerdasan`(`id_kecerdasan`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (7,7,20,'','Amatir','Palsu','Canggih','Ahli','Partikelir','Anasir','c');
insert  into `tb_kecerdasan`(`id_kecerdasan`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (8,7,20,'','Semua tumbuhan disinari matahari langsung memiliki bunga yang indah. Mawar adalah salah satu yang memiliki bunga yang indah. Kesimpulan yang tepat adalah...','Mawar disinari matahari langsung','Semua mawar memiliki bunga yang indah','Semua mawar disinari matahari','Mawar adalah tumbuhan','Mawar adalah bunga yang indah','d');
insert  into `tb_kecerdasan`(`id_kecerdasan`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (9,7,20,'','4 abad + 8 windu - 3 lustrum = ... Tahun','10','15','18','20','24','c');
insert  into `tb_kecerdasan`(`id_kecerdasan`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (10,7,20,'soalkubus.png','Berapa jumlah kubus?','10','15','18','20','6','e');

/*Table structure for table `tb_kelas` */

DROP TABLE IF EXISTS `tb_kelas`;

CREATE TABLE `tb_kelas` (
  `id_kelas` int NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `code` varchar(50) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `waktu_tes_kecermatan` int DEFAULT NULL COMMENT 'menit(dlm prgram convert ke milisecond)',
  `waktu_tes_kepribadian` int DEFAULT NULL COMMENT 'menit',
  `waktu_tes_kecerdasan` int DEFAULT NULL COMMENT 'menit',
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tb_kelas` */

insert  into `tb_kelas`(`id_kelas`,`id_paket_soal`,`kelas`,`deskripsi`,`code`,`tgl_mulai`,`tgl_selesai`,`waktu_tes_kecermatan`,`waktu_tes_kepribadian`,`waktu_tes_kecerdasan`) values (3,7,'Tes Kecermatan','Soal kecermatan Siap digunakan','12345dks','2021-04-13','2021-04-13',2,120,120);

/*Table structure for table `tb_kepribadian` */

DROP TABLE IF EXISTS `tb_kepribadian`;

CREATE TABLE `tb_kepribadian` (
  `id_kepribadian` int NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int NOT NULL,
  `bobot` int NOT NULL,
  `gambar` varchar(30) NOT NULL,
  `soal` text NOT NULL,
  `op_a` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_b` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_c` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_d` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `op_e` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jawab` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kepribadian`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tb_kepribadian` */

insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (1,8,20,'','Mamuju Oke Yaa','Semua benar','dicoba saja','akudandia','yaa','huuu','d');
insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (2,8,30,'','aku bisa coba soal kedua Yaa kjkfjaf kafkahfkhfk akhfkjhfkahf akfhkjahfka fkashfkjjhafkha fkahfkjjhfkashfkhaf kashfkahfkahfkhafkha fkahfkahfkahf akhfkahrewyrieahfk afkhdkfhdkmvbmv afkahfkhafksahfiweyrif kasfhkafhkas fdsfbbvjdvjafka fkahfkahfkahfka fbvmcb akf akfhkashfka fksjfhka fksdajfhkja fkahfkadshf ajfhakfjoweruekf dv kjdsadhkasjfhkaf kah lasalja kfh sfljasflakfjhf kafhdkhfdab vjdbvsjfka fkshf askhf','Semua benar','Benar ','Benar ','Benar','Benar','C');
insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (3,9,30,'','Mamuju Yaa','kedua dicoba','keduadicoba','mamauju dan dia dana','dana belem keluar','','c');
insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (4,7,30,'gambar1.jpg,gambar2.jpg','Gambar manakah yang membuat mata anda tertarik','satu','dua','tiga','empat','lima','a');
insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (5,7,30,'','pertanyaannya adalah jika bila 2?','satu','dua','tiga','empat','lima','a');
insert  into `tb_kepribadian`(`id_kepribadian`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (6,7,30,'gambar1.jpg','pertanyaannya adalah jika bila?','satu benar','2 benar','tiga benar','4 benar','5 benar','a');

/*Table structure for table `tb_login` */

DROP TABLE IF EXISTS `tb_login`;

CREATE TABLE `tb_login` (
  `id_login` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kl` varchar(20) NOT NULL,
  `agama` varchar(40) NOT NULL,
  `status` enum('aktif','tidak') NOT NULL,
  `level` enum('admin','peserta','','') NOT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tb_login` */

insert  into `tb_login`(`id_login`,`username`,`password`,`nama`,`tgl_lahir`,`jenis_kl`,`agama`,`status`,`level`) values (3,'Sukri Akbar','semua','semua','2021-04-01','laki-laki','islam','aktif','peserta');
insert  into `tb_login`(`id_login`,`username`,`password`,`nama`,`tgl_lahir`,`jenis_kl`,`agama`,`status`,`level`) values (6,'tesyaa','mamuju1234','macanga','2021-04-15','laki-laki','Islam','aktif','peserta');
insert  into `tb_login`(`id_login`,`username`,`password`,`nama`,`tgl_lahir`,`jenis_kl`,`agama`,`status`,`level`) values (7,'aswar','dites1234','aswarji','2021-10-04','laki-laki','islam','aktif','peserta');

/*Table structure for table `tb_nilai` */

DROP TABLE IF EXISTS `tb_nilai`;

CREATE TABLE `tb_nilai` (
  `id_nilai` int NOT NULL AUTO_INCREMENT,
  `id_login` int NOT NULL,
  `id_kelas` int NOT NULL,
  `jenis_tes` enum('kecermatan','kepribadian','kecerdasan') DEFAULT NULL,
  `jumlah_benar` int DEFAULT '0',
  `jumlah_salah` int DEFAULT '0',
  `total` varchar(40) NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Data for the table `tb_nilai` */

insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (1,6,3,NULL,0,0,'75');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (2,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (3,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (4,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (5,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (6,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (7,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (8,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (9,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (10,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (11,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (12,6,3,NULL,1,2,'1');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (13,6,3,NULL,2,1,'2');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (14,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (15,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (16,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (17,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (18,6,3,NULL,1,2,'1');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (19,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (20,6,3,NULL,1,2,'1');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (21,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (22,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (23,6,3,NULL,2,1,'60');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (24,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (25,6,3,NULL,0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (26,6,3,NULL,0,0,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (27,6,3,NULL,0,0,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (28,6,3,NULL,0,0,'1');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (29,6,3,NULL,0,0,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (30,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (31,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (32,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (33,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (34,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (35,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (36,6,3,'kecermatan',5,2,'3');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (37,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (38,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (39,6,3,'kecermatan',0,7,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (40,6,3,'kecermatan',0,7,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (41,6,3,'kecermatan',0,7,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (42,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (43,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (44,6,3,'kecermatan',5,2,'3');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (45,6,3,'kecermatan',4,3,'1');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (46,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (47,6,3,'kecermatan',0,7,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (48,6,3,'kecermatan',5,2,'3');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (49,6,3,'kepribadian',1,2,'30');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (50,6,3,'kecerdasan',0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (51,6,3,'kecerdasan',0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (52,6,3,'kecermatan',5,2,'3');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (53,6,3,'kepribadian',2,1,'60');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (54,6,3,'kecermatan',1,6,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (55,6,3,'kecermatan',0,7,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (56,6,3,'kecermatan',2,5,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (57,6,3,'kecermatan',10,2,'8');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (58,6,3,'kecermatan',10,2,'8');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (59,6,3,'kecermatan',10,2,'8');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (60,6,3,'kecermatan',12,0,'12');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (61,6,3,'kecerdasan',0,3,'0');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (62,6,3,'kepribadian',3,0,'90');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (63,6,3,'kecerdasan',3,0,'60');
insert  into `tb_nilai`(`id_nilai`,`id_login`,`id_kelas`,`jenis_tes`,`jumlah_benar`,`jumlah_salah`,`total`) values (64,6,3,'kecerdasan',4,0,'80');

/*Table structure for table `tb_paket_soal` */

DROP TABLE IF EXISTS `tb_paket_soal`;

CREATE TABLE `tb_paket_soal` (
  `id_paket_soal` int NOT NULL AUTO_INCREMENT,
  `nama_tes` text NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_paket_soal`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tb_paket_soal` */

insert  into `tb_paket_soal`(`id_paket_soal`,`nama_tes`,`deskripsi`) values (7,'Tes Kecermatan','Dicoba Tes Kecermatan');
insert  into `tb_paket_soal`(`id_paket_soal`,`nama_tes`,`deskripsi`) values (8,'Kedua tes','Dicoba tess');
insert  into `tb_paket_soal`(`id_paket_soal`,`nama_tes`,`deskripsi`) values (9,'Kedua Coba','Id Yang berbeda');

/*Table structure for table `tb_pertanyaan` */

DROP TABLE IF EXISTS `tb_pertanyaan`;

CREATE TABLE `tb_pertanyaan` (
  `id_pertanyaan` int NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int NOT NULL,
  `deskripsi_pertanyaan` text NOT NULL,
  `kolom_a` text NOT NULL,
  `kolom_b` text NOT NULL,
  `kolom_c` text NOT NULL,
  `kolom_d` text NOT NULL,
  `kolom_e` text NOT NULL,
  `waktu` int NOT NULL,
  PRIMARY KEY (`id_pertanyaan`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `tb_pertanyaan` */

insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (11,7,'Kolom 1','1','2','3','4','5',2);
insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (12,7,'Kolom 2','A','B','C','D','E',2);
insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (13,7,'Kolom 3','3','4','5','6','7',2);
insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (14,7,'Kolom 4','b','c','d','#','()',2);
insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (15,7,'Kolom 5','!','@','#','$','%',2);
insert  into `tb_pertanyaan`(`id_pertanyaan`,`id_paket_soal`,`deskripsi_pertanyaan`,`kolom_a`,`kolom_b`,`kolom_c`,`kolom_d`,`kolom_e`,`waktu`) values (16,7,'Kolom 5','!','@','#','$','()',2);

/*Table structure for table `tb_soal` */

DROP TABLE IF EXISTS `tb_soal`;

CREATE TABLE `tb_soal` (
  `id_soal` int NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int NOT NULL,
  `bobot` int NOT NULL,
  `gambar` varchar(30) NOT NULL,
  `soal` text NOT NULL,
  `op_a` text NOT NULL,
  `op_b` text NOT NULL,
  `op_c` text NOT NULL,
  `op_d` text NOT NULL,
  `op_e` text NOT NULL,
  `jawab` varchar(15) NOT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_soal` */

insert  into `tb_soal`(`id_soal`,`id_paket_soal`,`bobot`,`gambar`,`soal`,`op_a`,`op_b`,`op_c`,`op_d`,`op_e`,`jawab`) values (1,7,1,'gambar1.jpg,gambar2.jpg','Pertanyaannya adalah jika bila?','1','2','3','4','5','3');

/*Table structure for table `tb_waktu` */

DROP TABLE IF EXISTS `tb_waktu`;

CREATE TABLE `tb_waktu` (
  `id_waktu` int NOT NULL AUTO_INCREMENT,
  `jenis_tes` enum('kecermatan','kepribadian','kecerdasan') NOT NULL,
  `jumlah_waktu` int DEFAULT NULL,
  PRIMARY KEY (`id_waktu`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tb_waktu` */

insert  into `tb_waktu`(`id_waktu`,`jenis_tes`,`jumlah_waktu`) values (1,'kecerdasan',4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
